﻿using System;
using System.Collections;
using System.Collections.Generic;
//using FlyingWormConsole3.LiteNetLib;
using UnityEngine;
using UnityEngine.UI;
//using System.Security;


public class OptimizeThis : MonoBehaviour
{
    private Transform lookAtObject;

    public List<int> randomizeList = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 5, 5, 5, 5 };//Size of 14
    public List<int> removeFromList = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 5, 5, 5, 5 };//Size of 14
    public List<Image> imageList;//Size of 30
    public List<MeshRenderer> meshRendererList;//Size of 30
    private const string X_VALUE_KEY = "X_Value";
    private Coroutine coroutineMethodReference = null;
    /// <summary>
    /// Finding the object in initilization. but i would prefer to assign the object from inspector instead finding it and assign.
    /// In case object is spawning at run time then in that case i will call a fuction and assign its reference so without finding we can have access to that perticular Gameobject.
    /// </summary>
    private void Start()
    {
        GameObject targetObject = GameObject.Find("LookAtObject");
        if (targetObject)
        {
            lookAtObject = targetObject.GetComponent<Transform>();
        }
    }

    private void Update()
    {

    }


    /// <summary>
    /// In Late Update we are making the Camera look at the object called LookAtObject 
    /// </summary>
    private void LateUpdate()
    {
        if (lookAtObject == null)
            return;
        //lookAtObject = GameObject.Find("LookAtObject").GetComponent<Transform>();
        Camera.main.transform.LookAt(lookAtObject);
    }

    /// <summary>
    /// This method is used to get the value of X that was saved in Player Prefs
    /// No need to take any helper variable can 
    /// </summary>
    private int GetSavedXValue()
    {
        //int x = 0;
        //if (PlayerPrefs.HasKey("X_Value"))
        //{
        //    x = PlayerPrefs.GetInt("X_Value");
        //}
        //else
        //{
        //    x = 0;
        //}
        //return x;
        return PlayerPrefs.GetInt(X_VALUE_KEY, 0);
    }

    /// <summary>
    /// This is the OnClick method associated with a button. On Click it triggers an animation.
    /// Stopping the coroutine first the start the newer one
    /// </summary>
    public void OnClickDoAnimation()
    {
        if (coroutineMethodReference != null)
        {
            StopCoroutine(coroutineMethodReference);
        }
        coroutineMethodReference = StartCoroutine(CoroutineMethod());
    }

    /// <summary>
    /// This CoRoutine is liked to a button which is called repeatedly.
    /// </summary>
    private IEnumerator CoroutineMethod()
    {
        yield return new WaitForSeconds(5.0f);
        Debug.Log("This Coroutine is called every 5 seconds");
    }

    /// <summary>
    /// This method changes the color of all images to Black.
    /// Adding null reference check. it may happen we skipped any reference to assign
    /// </summary>
    private void UpdateImageColor()
    {
        foreach (var image in imageList)
        {
            if (image != null)
            {
                image.color = Color.black;
            }
        }
    }

    /// <summary>
    /// This method is used to enable/disable the mesh renderer of the assigned objects.
    /// Adding null reference check. it may happen we skipped any reference to assign
    /// </summary>
    private void UpdateMeshRenderer(bool status)
    {
        for (int i = 0; i < meshRendererList.Count; i++)
        {
            if (meshRendererList[i] != null)
            {
                meshRendererList[i].enabled = status;
            }
        }
    }

    /// <summary>
    /// This method is used to remove the number 5 from the List. Removing all- This will find all values and remove all in one go.
    /// </summary>
    private void RemoveNumberFromList()
    {
        //int x = removeFromList.Count;
        //for (int i = 0; i < x; i++)
        //{
        //    if (removeFromList[i] == 5)
        //    {
        //        removeFromList.RemoveAt(i);
        //    }
        //}
        removeFromList.RemoveAll(x => x == 5);
    }
}