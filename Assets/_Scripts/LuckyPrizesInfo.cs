﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LuckyPrizesInfo : MonoBehaviour
{
    bool isOpen = false;
    public bool isInitilized = false;
    public Button closeButton;
    void OnEnable()
    {
        isOpen = true;
    }

    public void Show()
    {
        if (isOpen)
        {
            return;
        }
        gameObject.SetActive(true);
        closeButton.interactable = true;
        LeanTween.scale(gameObject, Vector3.zero, 0f);
        LeanTween.scale(gameObject, Vector3.one, 0.35f).setEase(LeanTweenType.easeOutBack);
    }

    public void Close()
    {
        closeButton.interactable = false;
        LeanTween.scale(gameObject, Vector3.zero, 0.35f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            gameObject.SetActive(false);
        });
    }

    public void CloseImmidiate()
    {
        LeanTween.scale(gameObject, Vector3.zero, 0f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            gameObject.SetActive(false);
        });
    }


}

