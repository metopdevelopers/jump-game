﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DualCoin : MonoBehaviour
{
    public Button purchaseButton;
    private void OnEnable()
    {
        UpdateButtonStaus();
    }

    private void UpdateButtonStaus()
    {
        bool isPurchased = PlayerDataManager.Instance.GetDualcoinPurchaseStatus();
        purchaseButton.interactable = !isPurchased;
    }

    public void OnPurchase()
    {
        //At 1st index we have dual coin
        IAPHandler.Instance.BuyProduct(1);
        UpdateButtonStaus();
    }

    public void OnInfoPressed()
    {
        MessageBoxHandler.Instance.SetupMessageBox("Every coin worth became double.\nLast forever", "DUAL COINS!", false, false, false);
        MessageBoxHandler.Instance.Show();
    }
}
