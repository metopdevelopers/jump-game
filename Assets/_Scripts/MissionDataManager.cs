﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
class Mission
{
    public int activeIndex;
    public IMission mission;
}

public class MissionDataManager : MonoBehaviourSingleton<MissionDataManager>
{
    public const string ADVENTURE_DATA_KEY = "MissionDataManager";
    public const string SCORE_MULTIPLIER = "ScoreMultiplier";
    public int activeMissionIndex;
    List<IMission> missions;
    private IMission activeMission;

    public IMission ActiveMission { get => activeMission; set => activeMission = value; }

    private void Start()
    {
        activeMissionIndex = PlayerPrefs.GetInt(ADVENTURE_DATA_KEY, 0);
        missions = new List<IMission>();

        missions.Add(new MissionOne());
        missions.Add(new MissionTwo());
        missions.Add(new MissionThree());
        missions.Add(new MissionFour());
        missions.Add(new MissionFive());
        missions.Add(new MissionSix());
        missions.Add(new MissionSeven());
        missions.Add(new MissionEight());
        missions.Add(new MissionNine());
        missions.Add(new MissionTen());
        ActivateMission(activeMissionIndex);
    }

    void UpdateMissions()
    {
        if (ActiveMission == null)
            return;

        ActiveMission.UpdateMissions();
    }

    public void OnMissionAchieved()
    {
        PlayerPrefs.SetInt(ADVENTURE_DATA_KEY, PlayerPrefs.GetInt(ADVENTURE_DATA_KEY) + 1);
        PlayerPrefs.SetInt(SCORE_MULTIPLIER, PlayerPrefs.GetInt(SCORE_MULTIPLIER) + 1);
        ActivateMission(PlayerPrefs.GetInt(ADVENTURE_DATA_KEY));
    }

    public int GetCurrentMultiplier()
    {
        return PlayerPrefs.GetInt(SCORE_MULTIPLIER) + 1;
    }

    void ActivateMission(int missionId)
    {
        if (missions.Count > missionId)
        {
            activeMission = missions[missionId];
        }
        else 
        {
            activeMission = missions[0];
        }
    }
}
