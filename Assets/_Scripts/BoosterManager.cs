﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BoosterItemsData
{
    public List<BoosterItemData> boosterItemsData;
}

public class BoosterManager : MonoBehaviourSingleton<BoosterManager>
{
    public const string BOOSTER_KEY = "BoosterManager";
    public List<BoosterItem> boosterItems;
    public BoosterItemsData boosterItemsData;


    private void Start()
    {
        boosterItemsData = JsonUtility.FromJson<BoosterItemsData>(PlayerPrefs.GetString(BOOSTER_KEY, null));
        if (boosterItemsData == null)
        {

            boosterItemsData = new BoosterItemsData();
            boosterItemsData.boosterItemsData = new List<BoosterItemData>();

            BoosterItemData boosterItemData = new BoosterItemData();
            boosterItemData.boosterCount = 0;
            boosterItemData.isRealMoneyProduct = false;
            boosterItemData.boosterType = BoosterType.DFENCE;
            boosterItemData.coinsRequired = 150;
            boosterItemsData.boosterItemsData.Add(boosterItemData);

            boosterItemData = new BoosterItemData();
            boosterItemData.boosterCount = 0;
            boosterItemData.isRealMoneyProduct = false;
            boosterItemData.boosterType = BoosterType.BOOSTSCORE;
            boosterItemData.coinsRequired = 2000;

            boosterItemsData.boosterItemsData.Add(boosterItemData);

            boosterItemData = new BoosterItemData();
            boosterItemData.boosterCount = 0;
            boosterItemData.isRealMoneyProduct = false;
            boosterItemData.boosterType = BoosterType.BOOSTUP;
            boosterItemData.coinsRequired = 1000;
            boosterItemsData.boosterItemsData.Add(boosterItemData);

            Save();
        }
        //update ui according to the saved details
        for (int i = 0; i < boosterItems.Count; i++)
        {
            boosterItems[i].SetupData(boosterItemsData.boosterItemsData[i] , i);
        }
    }

    public void Save()
    {
        string boosterItemsDataJSON = JsonUtility.ToJson(boosterItemsData);
        PlayerPrefs.SetString(BOOSTER_KEY, boosterItemsDataJSON);
    }

    public void OnPurchase(int itemIndex, Currency currency , Action<bool , int> onPurchase)
    {
        if (currency == Currency.COINS)
        {
            int coinsCount = PlayerDataManager.Instance.GetCoinCount();

            if (coinsCount > 0 && coinsCount > boosterItemsData.boosterItemsData[itemIndex].coinsRequired/*upgradeItemsData.updgradeItemsData[itemIndex].GetCoinsNeeded()*/)
            {
                //User can but the item
                PlayerDataManager.Instance.AddCoins(-boosterItemsData.boosterItemsData[itemIndex].coinsRequired);
                boosterItemsData.boosterItemsData[itemIndex].boosterCount++;
                onPurchase(true , boosterItemsData.boosterItemsData[itemIndex].boosterCount);
                Save();
            }
            else
            {
                ShowAlert();
                onPurchase(false , 0);
            }
        }
    }

    private static void ShowAlert()
    {
        MessageBoxHandler.Instance.SetupMessageBox("Do not have sufficient funds. Need More Coins?", "OOPS!", false, false, true, () =>
        {
            Debug.Log("Will open coin shop");
            SelectionManager.Instance.OnBankTabClick(1);
        } , null , null , "BANK");
        MessageBoxHandler.Instance.Show();
    }

    private static void ShowFullMessage()
    {
        MessageBoxHandler.Instance.SetupMessageBox("You already have full power", "OOPS!", false, false, false);
        MessageBoxHandler.Instance.Show();
    }
}
