﻿using System;
using TMPro;
using UnityEngine;

public enum MessageBoxButtons
{
    OK,
    CANCEL,
    NOTHANKS,
    WATCH
}

public class MessageBoxHandler : MonoBehaviourSingleton<MessageBoxHandler>
{
    bool isOpen = false;
    public TextMeshProUGUI messageText;
    public TextMeshProUGUI headerText;
    public GameObject[] buttons;
    Action onOk, onCanceled, noThanks;
    MessageBoxButtons _messageBoxButtons;
    public GameObject messageBox;
    TextMeshProUGUI okText;
    public void SetupMessageBox(string message, string header, bool requiredWatch, bool requiredCancel, bool requiredNoThanks, Action onOk = null, Action onCanceled = null, Action noThanks = null, string okBottonName = "OK")
    {
        Debug.Log("Message : " + message);
        Debug.Log("Header : " + header);

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].SetActive(false);
        }
        this.onOk = onOk;
        this.onCanceled = onCanceled;
        this.noThanks = noThanks;

        buttons[0].SetActive(true);
        if (okText==null)
        {
            okText = buttons[0].GetComponentInChildren<TextMeshProUGUI>();
        }
        okText.SetText(okBottonName);
        if (requiredCancel)
        {
            buttons[3].SetActive(true);
        }
        if (requiredNoThanks)
        {
            buttons[1].SetActive(true);
        }
        if (requiredWatch)
        {
            buttons[2].SetActive(true);
        }
        Show();
        this.messageText.SetText(message);
        this.headerText.SetText(header);
    }

    public void OnOKPressed()
    {
        onOk?.Invoke();
        Close();
    }

    public void OnWatchPressed()
    {
        Close();
    }

    public void OnCancelPressed()
    {
        Close();
    }

    public void OnNoThanksPressed()
    {
        Close();
    }

    void OnEnable()
    {
        isOpen = true;
    }

    public void Show()
    {
        messageBox.SetActive(true);
        LeanTween.scale(messageBox, Vector3.zero, 0f);
        LeanTween.scale(messageBox, Vector3.one, 0.35f).setEase(LeanTweenType.easeOutBack);
    }

    public void Close()
    {
        LeanTween.scale(messageBox, Vector3.zero, 0.15f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            messageBox.SetActive(false);
        });
    }

    public void CloseImmidiate()
    {
        LeanTween.scale(gameObject, Vector3.zero, 0f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            messageBox.SetActive(false);
        });
    }

    private void OnDisable()
    {
        isOpen = false;
    }

    public void ComingSoon()
    {
        SetupMessageBox("Coming Soon", "Opps", false, false, false);
    }
}
