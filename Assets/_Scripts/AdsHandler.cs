﻿using UnityEngine.Advertisements;
using UnityEngine;
using System;
using UnityEngine.UI;

public class AdsHandler : MonoBehaviourSingleton<AdsHandler>, IUnityAdsListener
{
    string myPlacementId = "rewardedVideo";
    int[] coinsToReward = new int[] { 100, 200, 250, 300, 500 };
    int index;
    public static Action OnRewardedAdReady = delegate { };
    public Button rewardedAdButton;

    public Action onRewardedComplete;

    public enum ADWATCHED
    {
        FREECOINS,
        REVIVE,
        NONE
    }

    void Start()
    {
#if UNITY_ANDROID
        Advertisement.Initialize("3807651", true);
#else
        Advertisement.Initialize("3807650", true);
#endif
        Advertisement.AddListener(this);
        index = UnityEngine.Random.Range(0, coinsToReward.Length);
    }

    public void OnFreeCoinsClick()
    {
        Debug.Log("OnFreeCoinsClick");
        MessageBoxHandler.Instance.SetupMessageBox("Would you like to watch a video to recieve " + coinsToReward[index] + " Free Coins", "Earn a Reward!", false, false, true, () =>
          {
              ShowRewardedVideo(() =>
              {
                  Debug.Log("Give Reward");
                  PlayerDataManager.Instance.AddCoins(coinsToReward[index]);
              });
          });
        MessageBoxHandler.Instance.Show();
    }

    public void OnRevivePressed()
    {
        Debug.Log("OnRevivePressed");
        ShowRewardedVideo(() =>
        {
            Debug.Log("Ads seen can be revived");
            GameOverHandler.Instance.OnRestart();
        });
    }
    public void ShowRewardedVideo(Action onRewarded)
    {
        // Check if UnityAds ready before calling Show method:

        //if(GoogleAdsHandler.Instance.HasRewardedVideo())
        //{
        //    GoogleAdsHandler.Instance.ShowRewardedAd(OnRewarded  , OnRewardedFail);
        //    return;
        //}
        //else 
        onRewardedComplete = onRewarded;
        if (Advertisement.IsReady(myPlacementId))
        {
            Advertisement.Show(myPlacementId);
        }
        else
        {
            Debug.Log("Rewarded video is not ready at the moment! Please try again later!");
        }
    }

    private void OnRewardedFail()
    {
        MessageBoxHandler.Instance.SetupMessageBox("Would you like to watch a video to recieve " + coinsToReward[index] + " Free Coins", "Earn a Reward!", false, false, true);
        MessageBoxHandler.Instance.Show();
    }

    private void OnRewarded()
    {
        PlayerDataManager.Instance.AddCoins(coinsToReward[index]);
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            // Reward the user for watching the ad to completion.
            onRewardedComplete();
            onRewardedComplete = null;
            //PlayerDataManager.Instance.AddCoins(coinsToReward[index]);
        }
        else if (showResult == ShowResult.Skipped)
        {
            // Do not reward the user for skipping the ad.
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error.");
        }
    }

    public void OnUnityAdsReady(string placementId)
    {
        // If the ready Placement is rewarded, show the ad:
        if (placementId == myPlacementId)
        {
            // Optional actions to take when the placement becomes ready(For example, enable the rewarded ads button)
            index = UnityEngine.Random.Range(0, coinsToReward.Length);
            rewardedAdButton.interactable = true;
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
        rewardedAdButton.interactable = false;
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }

    // When the object that subscribes to ad events is destroyed, remove the listener:
    public void OnDestroy()
    {
        Advertisement.RemoveListener(this);
    }
}
