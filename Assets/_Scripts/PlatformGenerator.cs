﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviourSingleton<PlatformGenerator>
{
    public NormalTile normalTile;
    public MoveableTiles moveableTile;
    public BreakableTile breakableTile;
    public FakeTile fakeTile;

    const float MIN_GAP_Y = 1.6f;
    const float MAX_GAP_Y = 1;//MIN_GAP_Y * 2;

    const float MIN_GAP_X = 1.35f;
    float lastYPostion = 0;

    public Camera gamePlayCamera;

    private Vector2 screenBounds;

    float[] mainPositions = { -4.5f, 0, 4.5f };

    private float GetDeflection()
    {
        return Random.Range(-1.2f, 1.2f);
    }


    private void Start()
    {
        //Initilize();
        GameOverHandler.onSkip -= DestroyPath;
        GameOverHandler.onSkip += DestroyPath;
    }

    //public void Initilize()
    //{
    //    screenBounds = gamePlayCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, gamePlayCamera.transform.position.z));
    //    mainPositions = new float[] { -screenBounds.x, 0, screenBounds.x };
    //    Debug.Log("Screen Bounds" + screenBounds);
    //    lastYPostion = 0;
    //    SetTotalProbabilityWeight();
    //    StartCoroutine(GeneratePath());
    //}
    //public void DestroyPath()
    //{
    //    foreach (var item in transform.GetComponentsInChildren<Transform>())
    //    {
    //        if(item.name!=this.transform.name)
    //        {
    //            Destroy(item.gameObject);
    //        }
    //    }
    //}

    private int index = 0;
    string PATH = "PlatformPrefabs/Platform";
    string GEMS_PATH = "Gems/Gems";
    public void Initilize()
    {
        screenBounds = gamePlayCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, gamePlayCamera.transform.position.z));
        mainPositions = new float[] { -screenBounds.x, 0, screenBounds.x };

        //Debug.Log("Screen Bounds" + screenBounds);
        lastYPostion = 0;

        SpawnPredefinedDesign(GetPlatform());
        StartCoroutine("CheckPositionAndDeletePath");

        //StartCoroutine(GeneratePath());
    }

    private static int GetPlatform()
    {
        //return Random.Range(1, 21);
        return Random.Range(1, 2);
    }
    public List<GameObject> prefabContainer = new List<GameObject>();
    private void SpawnPredefinedDesign(int _index)
    {
        GameObject pathPrefab = Resources.Load(PATH + _index) as GameObject;
        pathPrefab = Instantiate(pathPrefab);
        pathPrefab.transform.SetParent(transform);
        pathPrefab.transform.position = new Vector3(pathPrefab.transform.position.x, lastYPostion + MAX_GAP_Y, pathPrefab.transform.position.z);

        ITile[] tiles = pathPrefab.GetComponentsInChildren<ITile>();
        //for (int i = 0; i < tiles.Length; i++)
        //{
        //    MonoBehaviour mb = tiles[i] as MonoBehaviour;
        //    mb.transform
        //}
        //foreach (var item in pathPrefab.GetComponentsInChildren<Transform>())
        foreach (var item in tiles)
        {
            MonoBehaviour mb = item as MonoBehaviour;
            if (mb.name != pathPrefab.name)
            {
                mb.transform.localScale = mb.transform.localScale * ScaleFactor.GetScaleFactor();
                if (mb.transform.position.y > lastYPostion)
                {
                    lastYPostion = mb.transform.position.y;
                }
                ClampTilesInBoundries(mb.gameObject);
            }
        }
        prefabContainer.Add(pathPrefab);

        //GameObject gemsPrefab = Resources.Load(GEMS_PATH + _index) as GameObject;
        //gemsPrefab = Instantiate(gemsPrefab);
        //gemsPrefab.transform.SetParent(transform);
        canConstructMoreTiles = true;
        //Debug.LogError("Last y position : " + lastYPostion);
    }

    public void DestroyPath()
    {
        if (this == null || transform == null)
            return;

        foreach (var item in transform.GetComponentsInChildren<Transform>())
        {
            if (item.name != this.transform.name)
            {
                Destroy(item.gameObject);
            }
        }
    }

    public void GenerateMoveableTile(float yPosition)
    {
        GameObject _moveableTile = Instantiate(moveableTile.gameObject);
        _moveableTile.transform.SetParent(transform);
        _moveableTile.transform.localScale = _moveableTile.transform.localScale * ScaleFactor.GetScaleFactor();
        int randonPick = Random.Range(0, mainPositions.Length);
        _moveableTile.transform.position = new Vector2(mainPositions[randonPick] + GetDeflection(), yPosition);
        _moveableTile.gameObject.SetActive(true);
        ClampTilesInBoundries(_moveableTile);

        SpriteRenderer sprite = _moveableTile.GetComponent<SpriteRenderer>();
        float width = sprite.bounds.size.x / 2f;

        MoveableTiles moveableTiles = _moveableTile.GetComponent<MoveableTiles>();
        if (moveableTiles)
        {
            moveableTiles.StartMovememt(-screenBounds.x + width, screenBounds.x - width);
        }
    }

    public void GenerateNormalTile(float yPosition)
    {
        GameObject _normalTile = Instantiate(normalTile.gameObject);
        _normalTile.transform.SetParent(transform);
        _normalTile.transform.localScale = _normalTile.transform.localScale * ScaleFactor.GetScaleFactor();
        int randonPick = Random.Range(0, mainPositions.Length);
        _normalTile.transform.position = new Vector2(mainPositions[randonPick] + GetDeflection(), yPosition);
        _normalTile.gameObject.SetActive(true);
        ClampTilesInBoundries(_normalTile);
    }

    public void GenerateBreakableTile(float yPosition)
    {
        GameObject _breakableTile = Instantiate(breakableTile.gameObject);
        _breakableTile.transform.SetParent(transform);
        _breakableTile.transform.localScale = _breakableTile.transform.localScale * ScaleFactor.GetScaleFactor();
        int randonPick = Random.Range(0, mainPositions.Length);
        _breakableTile.transform.position = new Vector2(mainPositions[randonPick] + GetDeflection(), yPosition);
        ClampTilesInBoundries(_breakableTile);
        _breakableTile.gameObject.SetActive(true);
    }

    public void GenerateFakeTile(float yPosition)
    {
        GameObject _fakeTile = Instantiate(fakeTile.gameObject);
        _fakeTile.transform.SetParent(transform);
        _fakeTile.transform.localScale = _fakeTile.transform.localScale * ScaleFactor.GetScaleFactor();
        int randonPick = Random.Range(0, mainPositions.Length);
        _fakeTile.transform.position = new Vector2(mainPositions[randonPick] + GetDeflection(), yPosition);
        ClampTilesInBoundries(_fakeTile);
        _fakeTile.gameObject.SetActive(true);
    }

    public void ClampTilesInBoundries(GameObject clampingGO)
    {
        Vector3 viewPort = clampingGO.transform.position;
        SpriteRenderer sprite = clampingGO.GetComponent<SpriteRenderer>();
        if (sprite)
        {
            float width = sprite.bounds.size.x / 2f;
            viewPort.x = Mathf.Clamp(viewPort.x, -screenBounds.x + width, screenBounds.x * 1 - width);
            clampingGO.transform.position = viewPort;
        }
    }

    public IEnumerator GeneratePath()
    {
        int MAX_ROWS = 100;

        for (int i = 0; i < MAX_ROWS; i++)
        {
            lastYPostion += Random.Range(MIN_GAP_Y, MAX_GAP_Y);
            float rand = Random.Range(0, totalWeight);
            int randomNumber = GetTileBasedOnRandomNumber((int)rand);

            switch (randomNumber)
            {
                case 0:
                    GenerateNormalTile(lastYPostion);
                    break;

                case 1:
                    GenerateBreakableTile(lastYPostion);
                    break;

                case 2:
                    GenerateFakeTile(lastYPostion);
                    break;

                case 3:
                    GenerateMoveableTile(lastYPostion);
                    break;

                default:
                    break;
            }
            if (i > 0 && i % 50 == 0)
            {
                yield return new WaitForSeconds(1f);
            }
            yield return new WaitForSeconds(0f);
        }
        canConstructMoreTiles = true;
    }

    public bool GetPrediction()
    {
        return Random.Range(1, 3) % 2 == 0;
    }

    int tileCounter = 0;
    bool canConstructMoreTiles = true;
    public void UpdateTileCounter()
    {
        //Debug.LogError("lastYPostion" + lastYPostion);
        //Debug.LogError("gamePlayCamera.transform.position.y : " + gamePlayCamera.transform.position.y);


        if (gamePlayCamera.transform.position.y > lastYPostion - 60 && canConstructMoreTiles)
        {
            canConstructMoreTiles = false;
            //StartCoroutine(GeneratePath());
            //Debug.LogError("Respawned");
            SpawnPredefinedDesign(GetPlatform());
        }
    }
    float speed = 1;
    List<GameObject> temp = new List<GameObject>();

    public IEnumerator CheckPositionAndDeletePath()
    {
        yield return new WaitForSeconds(1f / speed);

        if (gamePlayCamera != null || prefabContainer == null || prefabContainer.Count == 0)
            yield break;


        temp = new List<GameObject>();
        for (int i = 0; i < prefabContainer.Count; i++)
        {
            if (gamePlayCamera != null && (gamePlayCamera.transform.position.y - 100 > prefabContainer[i].transform.position.y))
            {
                Debug.Log("Adding : " + prefabContainer[i]);
                temp.Add(prefabContainer[i]);
            }
        }
        int count = temp.Count;
        for (int i = 0; i < count; i++)
        {
            if (prefabContainer.Contains(temp[i]))
            {
                prefabContainer.Remove(temp[i]);
            }
            Destroy(temp[i]);
            if (temp.Count > i)
            {
                temp.RemoveAt(i);
            }
            Debug.LogError("Removed");
        }
        StartCoroutine("CheckPositionAndDeletePath");
    }

    private void StopDeletion()
    {
        StopCoroutine("CheckPositionAndDeletePath");
    }

    float totalWeight = 0;
    void SetTotalProbabilityWeight()
    {
        float sum = 0;
        sum += TileWeightage.NORMAL_TILE;
        sum += TileWeightage.BREAKABLE_TILE;
        sum += TileWeightage.FAKE_TILE;
        sum += TileWeightage.MOVEABLE_TILE;
        totalWeight = sum;
    }

    int GetTileBasedOnRandomNumber(int randomNumber)
    {
        if (randomNumber <= TileWeightage.NORMAL_TILE)
            return 0;
        else if (randomNumber <= TileWeightage.NORMAL_TILE + TileWeightage.BREAKABLE_TILE)
            return 1;
        else if (randomNumber <= TileWeightage.NORMAL_TILE + TileWeightage.BREAKABLE_TILE + TileWeightage.FAKE_TILE)
            return 2;
        else if (randomNumber <= TileWeightage.NORMAL_TILE + TileWeightage.BREAKABLE_TILE + TileWeightage.FAKE_TILE + TileWeightage.MOVEABLE_TILE)
            return 3;

        return -1;
    }

    public void RegeneratePath()
    {
        DestroyPath();
        Initilize();
    }

}

public class TileWeightage
{
    public const int NORMAL_TILE = 75;
    public const int BREAKABLE_TILE = 10;
    public const int FAKE_TILE = 5;
    public const int MOVEABLE_TILE = 10;
}
