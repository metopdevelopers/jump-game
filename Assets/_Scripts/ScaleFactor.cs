﻿using UnityEngine;

public class ScaleFactor
{
    const float BASE_WIDTH = 768;
    const float BASE_HEIGHT = 1024;
    static float scaleFactor = 1;
    static bool isInitilized = false;
    public static void Initilize()
    {
        scaleFactor = (Screen.width / (float)Screen.height) / (BASE_WIDTH / BASE_HEIGHT);
    }

    public static float GetScaleFactor()
    {
        if(!isInitilized)
        {
            Initilize();
            isInitilized = true;
        }
        return scaleFactor;
    }
}
