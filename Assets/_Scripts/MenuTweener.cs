﻿using UnityEngine;

public class MenuTweener : MonoBehaviour
{
    public GameObject logo, rays , hills , building , breakedBuilding;
    void Start()
    {
        LeanTween.rotate(rays.GetComponent<RectTransform>(), 360 , 5.0f).setRepeat(-1);
        //LeanTween.rotateZ(rays, 360, 5.0f).setLoopPingPong();
        LeanTween.scale(logo, Vector3.zero, 0f);
        LeanTween.scale(logo, Vector3.one, 1.5f).setEaseOutBack();
        LeanTween.moveY(hills.GetComponent<RectTransform>(), -613, 1f).setEaseLinear();
        LeanTween.moveY(building.GetComponent<RectTransform>(), -492, 0.5f).setEaseLinear();
        LeanTween.moveY(breakedBuilding.GetComponent<RectTransform>(), 0, 1.2f).setEaseLinear();
    }
}
