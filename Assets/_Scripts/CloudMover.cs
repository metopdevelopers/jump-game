﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    FROMLEFT,
    FROMRIGHT,
    UPDOWN
}

public class CloudMover : MonoBehaviour
{
    public Direction direction;
    Vector2 originalPosition = Vector2.zero;

    private void Start()
    {
        originalPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        switch (direction)
        {
            case Direction.FROMLEFT:
                transform.Translate(Vector3.right * Time.deltaTime);
                if(transform.position.x > 12)
                {
                    transform.position = new Vector2( originalPosition.x , transform.position.y) ;
                }
                break;
            case Direction.FROMRIGHT:
                transform.Translate(Vector3.left * Time.deltaTime);
                if (transform.position.x < -12)
                {
                    transform.position = new Vector2(originalPosition.x, transform.position.y);
                }
                break;
            case Direction.UPDOWN:

               
                break;
            default:
                break;
        }
    }
}
