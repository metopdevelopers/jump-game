﻿using UnityEngine;
using UnityEngine.UI.Extensions;

public class BankUI : MonoBehaviour
{
    public HorizontalScrollSnap horizontalScrollSnap;
    public static bool needCoinsScreen;
    bool isOpen = false;
    LTDescr lT;
    void OnEnable()
    {
        isOpen = true;
    }

    public void Show()
    {
        if (isOpen)
        {
            return;
        }
        gameObject.SetActive(true);

       
        lT = LeanTween.delayedCall(0.1f, () => 
        {
            if (needCoinsScreen)
            {
                OpenCoinPage();
            }
            else
            {
                OpenGemsPage();
            }
        });

        //LeanTween.scale(gameObject, Vector3.zero, 0f);
        //LeanTween.scale(gameObject, Vector3.one, 0.35f).setEase(LeanTweenType.easeOutBack).setOnComplete(()=>{
        //horizontalScrollSnap.Reset();
        //});
    }

    public void OpenCoinPage()
    {
        horizontalScrollSnap.GoToScreen(1);
    }

    public void OpenGemsPage()
    {
        horizontalScrollSnap.GoToScreen(0);
    }

    public void Close()
    {
        LeanTween.scale(gameObject, Vector3.zero, 0.35f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            gameObject.SetActive(false);
        });
    }

    public void CloseImmidiate()
    {
        //LeanTween.scale(gameObject, Vector3.zero, 0f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        //{
        //    isOpen = false;
        //    gameObject.SetActive(false);
        //});
        isOpen = false;
        gameObject.SetActive(false);
    }
}


