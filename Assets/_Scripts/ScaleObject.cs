﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleObject : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        LeanTween.scale(gameObject, Vector2.one * 1.2f, 0.35f).setLoopPingPong();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
