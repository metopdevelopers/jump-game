﻿using UnityEngine;

public class MystryBox : MonoBehaviour
{
    public int coinsRequired = 300;

    public void OnPurchase()
    {
        int coinsCount = PlayerDataManager.Instance.GetCoinCount();
        if (coinsCount > 0 && coinsCount > coinsRequired)
        {
            PlayerDataManager.Instance.AddGifts(1);
        }
        else
        {
            ShowAlert();
        }
    }

    private static void ShowAlert()
    {
        MessageBoxHandler.Instance.SetupMessageBox("You need more Coins. Play to collect coins, or get more from the bank.", "OOPS!", false, false, true, () =>
        {
            Debug.Log("Will open coin shop");
            SelectionManager.Instance.OnBankTabClick(1);
        }, null, null, "BANK");
        MessageBoxHandler.Instance.Show();
    }
}
