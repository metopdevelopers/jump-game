﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeItemManager : MonoBehaviourSingleton<UpgradeItemManager>
{
    public UpgradeItemHandler[] upgradeItemHandlers;
    public const string UPGRADE_KEY = "UpgradeItemManager";
    public const int MAX_ITEMS = 8;
    public Sprite onSprite, offSprite;

    public UpgradeItemsData upgradeItemsData;


    private void Start()
    {
        upgradeItemsData = JsonUtility.FromJson<UpgradeItemsData>(PlayerPrefs.GetString(UPGRADE_KEY, null));
        if (upgradeItemsData == null)
        {
            upgradeItemsData = new UpgradeItemsData();
            upgradeItemsData.updgradeItemsData = new List<UpgradeItemData>();

            for (int i = 0; i < MAX_ITEMS; i++)
            {
                UpgradeItemData upgradeItemData = new UpgradeItemData();
                upgradeItemData.powerLevel = 0;
                upgradeItemData.addonTime = new int[] { 1, 2, 3, 4, 5, 6, 7 };
                //upgradeItemData.coinsNeeded = new int[MAX_ITEMS - 1];
                //for (int j = 0; j < MAX_ITEMS - 1; j++)
                //{
                //    upgradeItemData.coinsNeeded[j] = (int)(300 + (j * 50));
                //}
                upgradeItemData.coinsNeeded = new int[] { 200, 600, 1800, 2500, 7500, 9600, 25000 };

                upgradeItemsData.updgradeItemsData.Add(upgradeItemData);
            }
            Save();
        }

        //update ui according to the saved details
        for (int i = 0; i < upgradeItemHandlers.Length; i++)
        {
            upgradeItemHandlers[i].UpdateUI(upgradeItemsData.updgradeItemsData[i]);
        }

    }

    public void Save()
    {
        string upgradeItemsDataJSON = JsonUtility.ToJson(upgradeItemsData);
        PlayerPrefs.SetString(UPGRADE_KEY, upgradeItemsDataJSON);
    }

    public void OnPurchase(int itemIndex, Currency currency)
    {

        if (currency == Currency.COINS)
        {
            int coinsCount = PlayerDataManager.Instance.GetCoinCount();
            if (upgradeItemsData.updgradeItemsData[itemIndex].powerLevel >= upgradeItemsData.updgradeItemsData[itemIndex].addonTime.Length)
            {
                ShowFullMessage();
                return;
            }

            Debug.Log("Gems i have : " + coinsCount + "itemIndex : " + itemIndex);
            Debug.Log("Gems required : " + upgradeItemsData.updgradeItemsData[itemIndex].GetCoinsNeeded());
            if (PlayerDataManager.Instance.GetCoinCount() > 0 && PlayerDataManager.Instance.GetCoinCount() >= upgradeItemsData.updgradeItemsData[itemIndex].GetCoinsNeeded())
            {
                //User can but the item
                PlayerDataManager.Instance.AddCoins(-upgradeItemsData.updgradeItemsData[itemIndex].GetCoinsNeeded());

                Debug.Log("Length : " + upgradeItemsData.updgradeItemsData.Count);
                upgradeItemsData.updgradeItemsData[itemIndex].powerLevel++;

                UpgradeItemData upgradeItemData = upgradeItemsData.updgradeItemsData[itemIndex];
                Debug.Log(upgradeItemData.powerLevel);
                upgradeItemHandlers[itemIndex].UpdateUI(upgradeItemData);
                Save();
            }
            else
            {
                ShowAlert(1);
            }
        }
        else if (currency == Currency.GEMS)
        {
            int gemsCount = PlayerDataManager.Instance.GetGemsCount();
            Debug.Log("Gems i have : " + gemsCount);
            Debug.Log("Gems required : " + upgradeItemsData.updgradeItemsData[itemIndex].GetCoinsNeeded());

            if (gemsCount > 0 && gemsCount >= upgradeItemsData.updgradeItemsData[itemIndex].GetCoinsNeeded())
            {
                //User can but the item
                PlayerDataManager.Instance.AddGems(-upgradeItemsData.updgradeItemsData[itemIndex].GetCoinsNeeded());
                upgradeItemsData.updgradeItemsData[itemIndex].powerLevel++;
                if (upgradeItemsData.updgradeItemsData[itemIndex].powerLevel >= upgradeItemsData.updgradeItemsData[itemIndex].addonTime.Length)
                {
                    ShowFullMessage();
                    return;
                }

                upgradeItemHandlers[itemIndex].UpdateUI(upgradeItemsData.updgradeItemsData[itemIndex]);
                Save();
            }
            else
            {
                ShowAlert(0);
            }
        }

    }

    private static void ShowAlert(int index)
    {
        MessageBoxHandler.Instance.SetupMessageBox("You need more Coins. Play to collect coins, or get more from the bank.", "OOPS!", false, false, true, () =>
        {
            Debug.Log("Will open coin shop");
            SelectionManager.Instance.OnBankTabClick(index);
        }, null , null , "BANK");
        MessageBoxHandler.Instance.Show();
    }

    private static void ShowFullMessage()
    {
        MessageBoxHandler.Instance.SetupMessageBox("You already have full power", "OOPS!", false, false, false);
        MessageBoxHandler.Instance.Show();
    }
}
