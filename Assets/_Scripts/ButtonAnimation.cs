﻿using UnityEngine;

public class ButtonAnimation : MonoBehaviour
{
    void Start()
    {
        InvokeRepeating("AnimatePlay", 0, 4f);
    }
    void AnimatePlay()
    {
        LeanTween.scaleX(gameObject, 0.7f, 0.2f).setLoopPingPong(1);
        LeanTween.scaleY(gameObject, 1.2f, 0.2f).setLoopPingPong(1);
    }

    private void OnDisable()
    {
        CancelInvoke("AnimatePlay");                                                                                                            
    }
}
