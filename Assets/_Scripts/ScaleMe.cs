﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleMe : MonoBehaviour
{
    void Start()
    {
        transform.localScale = transform.localScale * ScaleFactor.GetScaleFactor();
    }

}
