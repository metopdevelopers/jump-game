﻿
public class MissionSix : IMission
{
    public bool isCoinsCollected;
    public bool isHeroUnlocked;
    public bool megnetUsed;

    public string[] missions = new string[] { "Collect 200 Coins", "Unlock a new Hero", "Use Magnetism" };
    public int[] prices = { 1300, 1300, 1300 };
    public bool[] achievementStaus = new bool[] { false, false, false };

    public void UpdateMissions()
    {

    }

    public string[] GetSubMissions()
    {
        return missions;
    }

    public int[] GetPurchaseAmounts()
    {
        return prices;
    }

    public bool[] GetAchievementStatus()
    {
        return achievementStaus;

    }
}
