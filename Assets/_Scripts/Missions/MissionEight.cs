﻿
public class MissionEight : IMission
{
    public bool IsBoostScoreUsed;
    public bool IsAthenaUnlocked;
    public bool isDoubleFunUpgraded;

    public string[] missions = new string[] { "Use Boost Score", "Unlock Athena", "Upgrade Double Fun" };
    public int[] prices = { 1700, 1700, 1700 };
    public bool[] achievementStaus = new bool[] { false, false, false };


    public void UpdateMissions()
    {

    }
    public string[] GetSubMissions()
    {
        return missions;
    }

    public int[] GetPurchaseAmounts()
    {
        return prices;
    }

    public bool[] GetAchievementStatus()
    {
        return achievementStaus;

    }
}
