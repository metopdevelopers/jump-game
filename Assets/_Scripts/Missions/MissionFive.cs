﻿
public class MissionFive : IMission
{
    public bool isDoublefunUsed;
    public bool isShardCollected;
    public bool isThreeVideoatched;

    public string[] missions = new string[] { "Use Double Fun", "Watch 3 videos for coins", "Collect a Shard" };
    public int[] prices = { 1100, 1100, 1100 };
    public bool[] achievementStaus = new bool[] { false, false, false };

    public void UpdateMissions()
    {

    }
    public string[] GetSubMissions()
    {
        return missions;
    }

    public int[] GetPurchaseAmounts()
    {
        return prices;
    }

    public bool[] GetAchievementStatus()
    {
        return achievementStaus;

    }
}
