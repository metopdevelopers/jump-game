﻿
public class MissionSeven : IMission
{
    public bool isEnterInColosolMode;
    public bool isAnUpgradeEnhanced;
    public bool isCoverTheDistance;

    public string[] missions = new string[] { "Enter Collosal Mode", "Enhance an Upgrade", "Reach 500m in a single game" };
    public int[] prices = { 1500, 500, 1500 };
    public bool[] achievementStaus = new bool[] { false, false, false };

    public void UpdateMissions()
    {

    }
    public string[] GetSubMissions()
    {
        return missions;
    }

    public int[] GetPurchaseAmounts()
    {
        return prices;
    }

    public bool[] GetAchievementStatus()
    {
        return achievementStaus;

    }
}
