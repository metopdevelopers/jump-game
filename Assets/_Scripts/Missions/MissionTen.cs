﻿
public class MissionTen : IMission
{
    public bool isHadesUnlocked;
    public int isCoinsCollected;
    public bool isJumpedOnEnemy;

    public string[] missions = new string[] { "Unlock Hades", "Reach 1500m in a single game", "Collect 250 coins in a single game" };
    public int[] prices = { 2500, 2500, 2500 };
    public bool[] achievementStaus = new bool[] { false, false, false };

    public void UpdateMissions()
    {

    }
    public string[] GetSubMissions()
    {
        return missions;
    }

    public int[] GetPurchaseAmounts()
    {
        return prices;
    }

    public bool[] GetAchievementStatus()
    {
        return achievementStaus;

    }
}
