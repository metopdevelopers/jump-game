﻿
public class MissionThree : IMission
{
    public bool isFortunaMystryBoxOpened;
    public bool isUpgradedPurchased;
    public bool isTwovideoWatched;

    public string[] missions = new string[] { "Open a Fortunas Mystery Box", "Buy an Upgrade", "Watch two videos for coins" };
    public int[] prices = { 700, 700, 700 };
    public bool[] achievementStaus = new bool[] { false, false, false };

    public void UpdateMissions()
    {

    }
    public string[] GetSubMissions()
    {
        return missions;
    }

    public int[] GetPurchaseAmounts()
    {
        return prices;
    }

    public bool[] GetAchievementStatus()
    {
        return achievementStaus;
    }
}
