﻿
public class MissionNine : IMission
{
    public bool isCyclopsUnlocked;
    public bool reachDesiredDistance;
    public bool isVideoWatched;

    public string[] missions = new string[] { "Unlock Cyclops", "Reach 1000m in a single game", "Watch a video for coins" };
    public int[] prices = { 2000, 2000, 2000 };
    public bool[] achievementStaus = new bool[] { false, false, false };

    public void UpdateMissions()
    {

    }

    public string[] GetSubMissions()
    {
        return missions;
    }

    public int[] GetPurchaseAmounts()
    {
        return prices;
    }

    public bool[] GetAchievementStatus()
    {
        return achievementStaus;

    }
}
