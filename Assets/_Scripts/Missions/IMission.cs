﻿public interface IMission
{
    void UpdateMissions();
    string[] GetSubMissions();
    int[] GetPurchaseAmounts();
    bool[] GetAchievementStatus(); 
}
