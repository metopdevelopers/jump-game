﻿
public class MissionFour : IMission
{
    public bool isSaveMePurchased;
    public bool isJumpedOnEnemy;
    public bool isFireThumpUsed;

    public string[] missions = new string[] { "Buy a Save Me", "Jump on an Enemy", "Use Fire Thump" };
    public int[] prices = { 900, 900, 900 };
    public bool[] achievementStaus = new bool[] { false, false, false };

    public void UpdateMissions()
    {

    }
    public string[] GetSubMissions()
    {
        return missions;
    }

    public int[] GetPurchaseAmounts()
    {
        return prices;
    }

    public bool[] GetAchievementStatus()
    {
        return achievementStaus;

    }
}
