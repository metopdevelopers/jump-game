﻿
public class MissionOne : IMission
{
    public bool IsGamePlayed;
    public int reachDesiredDistance;
    public bool videoWatched;

    public string[] missions = new string[] { "Play a Game", "Reach 300m in a single game", "Watch a video for coins" };
    public int[] prices = { 300, 300, 300 };
    public bool[] achievementStaus = new bool[] { false, false, false };
    public string[] GetSubMissions()
    {
        return missions;
    }

    public int[] GetPurchaseAmounts()
    {
        return prices;
    }

    public void UpdateMissions()
    {

    }

    public bool[] GetAchievementStatus()
    {
        return achievementStaus;
    }
}
