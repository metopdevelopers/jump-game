﻿
public class MissionTwo : IMission
{
    public bool isArtifactPurchased;
    public int isCoinsCollected;
    public bool isJumpedOnEnemy;

    public string[] missions = new string[] { "Buy an Artifact", "Collect 50 coins in a single game", "Jump on an Enemy" };
    public int[] prices = { 500, 500, 500 };
    public bool[] achievementStaus = new bool[] { false, false, false };

    public void UpdateMissions()
    {

    }
    public string[] GetSubMissions()
    {
        return missions;
    }

    public int[] GetPurchaseAmounts()
    {
        return prices;
    }

    public bool[] GetAchievementStatus()
    {
        return achievementStaus;
    }
}
