﻿using System.Collections.Generic;

[System.Serializable]
public class ProductDetail
{
    public string IAPKEY = string.Empty;
    public List<ProductData> productData = null;
}
