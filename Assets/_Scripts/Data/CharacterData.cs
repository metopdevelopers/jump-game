﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum Currency
{
    NONE,
    COINS,
    GEMS,
    GIFT
}

public enum CharacterType
{
    NONE,
    STANDARD,
    SECRET
}

public enum PurchaseStatus
{
    NONE,
    NOTPURCHASED,
    PURCHASED,
    UNLOCABLE
}

[System.Serializable]
public class GameCharacter
{
    public Currency currency;
    public CharacterType characterType;
    public PurchaseStatus purchaseStatus;
    public int amountRequiredToPurchase;
    public string characterName;
    public int unlockedLevel = 0;
    public string[] benefits;
    public bool isSelected;
}

[System.Serializable]
public class CharacterDataSet
{
    public List<GameCharacter> GameCharacters;

    public void SetCharacterPurchased(int index)
    {
        SetSelected();
        GameCharacters[index].purchaseStatus = PurchaseStatus.PURCHASED;
        GameCharacters[index].isSelected = true;
    }

    public void SetSelected()
    {
        GameCharacters.ForEach(
            (GameCharacter character) =>
            {
                if (character.isSelected)
                {
                    character.isSelected = false;
                }
            }
        );
    }

    public void SelectCharacter(int index)
    {
        SetSelected();
        GameCharacters[index].isSelected = true;
    }

    public int GetSelectedCharacterIndex()
    {
        int selectedIndex = 0;
        selectedIndex = GameCharacters.FindIndex(obj => obj.isSelected == true);
        return selectedIndex;
    }
}

public class CharacterData : MonoBehaviourSingleton<CharacterData>
{
    CharacterDataSet characterDataSet = null;
    public Sprite[] characters;
    public RuntimeAnimatorController[] charactersAnimator;

    private const string GameCharactersKey = "GameCharactersKey";

    private void Awake()
    {
        Initilize();
    }

    void Initilize()
    {
        string _characterData = PlayerPrefs.GetString(GameCharactersKey, null);
        characterDataSet = JsonUtility.FromJson<CharacterDataSet>(_characterData);
        if (characterDataSet == null)
        {
            characterDataSet = new CharacterDataSet();
            characterDataSet.GameCharacters = new List<GameCharacter>();
            SetupCharacterData();
        }
    }

    public void SetupCharacterData()
    {
        GameCharacter gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[0];
        gameCharacter.currency = Currency.NONE;
        gameCharacter.amountRequiredToPurchase = 0;
        gameCharacter.purchaseStatus = PurchaseStatus.PURCHASED;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.isSelected = true;
        gameCharacter.benefits = new string[] { "+1 Second to Colossal Mode" };
        characterDataSet.GameCharacters.Add(gameCharacter);

        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[1];
        gameCharacter.currency = Currency.COINS;
        gameCharacter.amountRequiredToPurchase = 1000;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.benefits = new string[] { "+1 Second to Magnetism" };

        characterDataSet.GameCharacters.Add(gameCharacter);


        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[2];
        gameCharacter.currency = Currency.COINS;
        gameCharacter.amountRequiredToPurchase = 3000;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.benefits = new string[] { "+2 Seconds to Tricky Jump" };

        characterDataSet.GameCharacters.Add(gameCharacter);


        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[3];
        gameCharacter.currency = Currency.GEMS;
        gameCharacter.amountRequiredToPurchase = 10;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.benefits = new string[] { "Begin each round with D-Fence", "+10% Coin Bonus" };

        characterDataSet.GameCharacters.Add(gameCharacter);


        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[4];
        gameCharacter.currency = Currency.COINS;
        gameCharacter.amountRequiredToPurchase = 7500;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.benefits = new string[] { "+1 Second to Fire Thump", "+1 Second to Slow Flow" };

        characterDataSet.GameCharacters.Add(gameCharacter);


        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[5];
        gameCharacter.currency = Currency.GEMS;
        gameCharacter.amountRequiredToPurchase = 50;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.benefits = new string[] { "+2 Second to Slow Flow", "Can Jump on Magic Platforms", "+50% Coin Bonus" };

        characterDataSet.GameCharacters.Add(gameCharacter);


        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[6];
        gameCharacter.currency = Currency.GEMS;
        gameCharacter.amountRequiredToPurchase = 25;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.benefits = new string[] { "+2 Seconds to Attack Mode", "+25% Coin Bonus" };

        characterDataSet.GameCharacters.Add(gameCharacter);


        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[7];
        gameCharacter.currency = Currency.GEMS;
        gameCharacter.amountRequiredToPurchase = 25;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.benefits = new string[] { "+1 Second to Stop Dead", "Can Jump on Enemies", "+25% Coin Bonus" };

        characterDataSet.GameCharacters.Add(gameCharacter);


        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[8];
        gameCharacter.currency = Currency.GEMS;
        gameCharacter.amountRequiredToPurchase = 100;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;

        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.benefits = new string[] { "+2 Second to all Powerups", "Can Jump on Magic Platforms", "+75% Coin Bonus" };

        characterDataSet.GameCharacters.Add(gameCharacter);


        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[9];
        gameCharacter.currency = Currency.NONE;
        gameCharacter.amountRequiredToPurchase = 5;
        gameCharacter.purchaseStatus = PurchaseStatus.NONE;
        gameCharacter.characterType = CharacterType.SECRET;
        gameCharacter.unlockedLevel = 5;
        gameCharacter.benefits = new string[] { "+1 Second to Attack Mode" };

        characterDataSet.GameCharacters.Add(gameCharacter);

        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[10];
        gameCharacter.currency = Currency.COINS;
        gameCharacter.amountRequiredToPurchase = 50000;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.unlockedLevel = 0;
        gameCharacter.benefits = new string[] { "+3 seconds to all Powerups", "Begin each round with Aegis Shield" };
        characterDataSet.GameCharacters.Add(gameCharacter);

        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[11];
        gameCharacter.currency = Currency.COINS;
        gameCharacter.amountRequiredToPurchase = 5000;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.unlockedLevel = 0;
        gameCharacter.benefits = new string[] { "+1 Seconds to Double Fun" };
        characterDataSet.GameCharacters.Add(gameCharacter);

        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[12];
        gameCharacter.currency = Currency.COINS;
        gameCharacter.amountRequiredToPurchase = 10000;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.SECRET;
        gameCharacter.unlockedLevel = 0;
        gameCharacter.benefits = new string[] { "+2 Seconds to Magnetism", "+1 Second to Fire Thump" };

        characterDataSet.GameCharacters.Add(gameCharacter);

        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[13];
        gameCharacter.currency = Currency.COINS;
        gameCharacter.amountRequiredToPurchase = 20000;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.unlockedLevel = 0;
        gameCharacter.benefits = new string[] { "+2 Seconds to all Powerups", "Begin each round with a +350m Head Start" };

        characterDataSet.GameCharacters.Add(gameCharacter);

        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[14];
        gameCharacter.currency = Currency.GEMS;
        gameCharacter.amountRequiredToPurchase = 25;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.unlockedLevel = 0;
        gameCharacter.benefits = new string[] { "Bounce 10% Higher", "+25% Coin Bonus" };

        gameCharacter = new GameCharacter();
        gameCharacter.characterName = StaticString.CHARACTER_NAMES[15];
        gameCharacter.currency = Currency.GEMS;
        gameCharacter.amountRequiredToPurchase = 150;
        gameCharacter.purchaseStatus = PurchaseStatus.UNLOCABLE;
        gameCharacter.characterType = CharacterType.STANDARD;
        gameCharacter.unlockedLevel = 0;
        gameCharacter.benefits = new string[] { "+3 Seconds to all Powerups", "3x Score Multiplier", "Double Coin Bonus" };

        characterDataSet.GameCharacters.Add(gameCharacter);

        Save();

    }

    public void Save()
    {
        string characterData = JsonUtility.ToJson(characterDataSet);
        PlayerPrefs.SetString(GameCharactersKey, characterData);
        Debug.Log("Character Details Saved");
    }

    public CharacterDataSet GetCharacterData()
    {
        string _characterData = PlayerPrefs.GetString(GameCharactersKey, null);
        characterDataSet = JsonUtility.FromJson<CharacterDataSet>(_characterData);
        return characterDataSet;
    }

    public Sprite GetPlayerSprite()
    {
        int selectedIndex = 0;
        selectedIndex = characterDataSet.GameCharacters.FindIndex(obj => obj.isSelected == true);
        selectedIndex = Mathf.Clamp(selectedIndex , 0 , characterDataSet.GameCharacters.Count);
        //Debug.LogError("Selected Index : " + selectedIndex);
        return characters[selectedIndex];
    }

    public RuntimeAnimatorController GetPlayerAnimator()
    {
        int selectedIndex = 0;
        selectedIndex = characterDataSet.GameCharacters.FindIndex(obj => obj.isSelected == true);
        selectedIndex = Mathf.Clamp(selectedIndex, 0, characterDataSet.GameCharacters.Count);
        //Debug.LogError("Selected Index : " + selectedIndex);
        return charactersAnimator[selectedIndex];
    }

    public void UpdateStatusAfterPurchase(int playerIndex)
    {

    }
}
