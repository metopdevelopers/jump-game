﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public enum PurchaseIdentifier
{
    NONE,
    STARTERDEAL,
    DUALCOIN,
    BUNCHOFGEMS,
    PILEOFGEMS,
    POUCHOFGEMS,
    BOXOFGEMS,
    BUNCHOFCOINS,
    STACKOFCOINS,
    BAGOFCOINS,
    CHESTOFCOINS
}

public class IAPHandler : MonoBehaviourSingleton<IAPHandler>
{
    IAPDetails iapDetails = null;
    public const string IAP_KEY = "IAP";

    void Start()
    {
        iapDetails = JsonUtility.FromJson<IAPDetails>(PlayerPrefs.GetString(IAP_KEY, null));
        if (iapDetails == null || (iapDetails != null && iapDetails.purchaseItemDetails != null && iapDetails.purchaseItemDetails.Count == 0))
        {
            iapDetails = new IAPDetails();
            SetupIAP();
        }
        Debug.Log("Product JSON : " + JsonUtility.ToJson(iapDetails));

    }

    public void SetupIAP()
    {
        iapDetails.purchaseItemDetails = new List<ProductDetail>();
        iapDetails.purchaseItemDetails = GetProductDetails();
        string iapJSON = JsonUtility.ToJson(iapDetails);
        PlayerPrefs.SetString(IAP_KEY, iapJSON);
    }

    public List<ProductDetail> GetProductDetails()
    {
        List<ProductDetail> products = new List<ProductDetail>();
        //1st Product

        ProductDetail productDetail = new ProductDetail();
        productDetail.IAPKEY = PurchaseIdentifier.STARTERDEAL.ToString();
        productDetail.productData = new List<ProductData>();

        ProductData productData = new ProductData();

        productData.currency = Currency.COINS;
        productData.count = 5000;
        productDetail.productData.Add(productData);

        productData = new ProductData();

        productData.currency = Currency.GIFT;
        productData.count = 3;
        productDetail.productData.Add(productData);

        productData = new ProductData();

        productData.currency = Currency.GEMS;
        productData.count = 5;
        productDetail.productData.Add(productData);

        products.Add(productDetail);


        //2nd Product
        productDetail = new ProductDetail();
        productDetail.IAPKEY = PurchaseIdentifier.DUALCOIN.ToString();
        productDetail.productData = new List<ProductData>();

        productData = new ProductData();
        productData.currency = Currency.NONE;
        productDetail.productData.Add(productData);
        products.Add(productDetail);

        //3rd Product
        productDetail = new ProductDetail();
        productDetail.IAPKEY = PurchaseIdentifier.BUNCHOFGEMS.ToString();
        productDetail.productData = new List<ProductData>();

        productData = new ProductData();
        productData.currency = Currency.GEMS;
        productData.count = 30;
        productDetail.productData.Add(productData);
        products.Add(productDetail);

        //4th Product
        productDetail = new ProductDetail();
        productDetail.IAPKEY = PurchaseIdentifier.PILEOFGEMS.ToString();
        productDetail.productData = new List<ProductData>();

        productData = new ProductData();
        productData.currency = Currency.GEMS;
        productData.count = 100;
        productDetail.productData.Add(productData);
        products.Add(productDetail);

        //5th Product
        productDetail = new ProductDetail();
        productDetail.IAPKEY = PurchaseIdentifier.POUCHOFGEMS.ToString();
        productDetail.productData = new List<ProductData>();

        productData = new ProductData();
        productData.currency = Currency.GEMS;
        productData.count = 250;
        productDetail.productData.Add(productData);
        products.Add(productDetail);

        //6th Product
        productDetail = new ProductDetail();
        productDetail.IAPKEY = PurchaseIdentifier.BOXOFGEMS.ToString();
        productDetail.productData = new List<ProductData>();

        productData = new ProductData();
        productData.currency = Currency.GEMS;
        productData.count = 625;
        productDetail.productData.Add(productData);
        products.Add(productDetail);

        //7th Product
        productDetail = new ProductDetail();
        productDetail.IAPKEY = PurchaseIdentifier.BUNCHOFCOINS.ToString();
        productDetail.productData = new List<ProductData>();

        productData = new ProductData();
        productData.currency = Currency.COINS;
        productData.count = 15000;
        productDetail.productData.Add(productData);
        products.Add(productDetail);

        //8th Product
        productDetail = new ProductDetail();
        productDetail.IAPKEY = PurchaseIdentifier.STACKOFCOINS.ToString();
        productDetail.productData = new List<ProductData>();

        productData = new ProductData();
        productData.currency = Currency.COINS;
        productData.count = 60000;
        productDetail.productData.Add(productData);
        products.Add(productDetail);

        //9th Product
        productDetail = new ProductDetail();
        productDetail.IAPKEY = PurchaseIdentifier.BAGOFCOINS.ToString();
        productDetail.productData = new List<ProductData>();

        productData = new ProductData();
        productData.currency = Currency.COINS;
        productData.count = 100000;
        productDetail.productData.Add(productData);
        products.Add(productDetail);


        //10th Product
        productDetail = new ProductDetail();
        productDetail.IAPKEY = PurchaseIdentifier.CHESTOFCOINS.ToString();
        productDetail.productData = new List<ProductData>();

        productData = new ProductData();
        productData.currency = Currency.COINS;
        productData.count = 315000;
        productDetail.productData.Add(productData);
        products.Add(productDetail);

        return products;
    }

    public void OnPurchaseSuccessFul(int productIndex)
    {
        if (iapDetails.purchaseItemDetails.Count < productIndex)
        {
            Debug.Log("Something went wrong");
            return;
        }

        ProductDetail productDetail = iapDetails.purchaseItemDetails[productIndex];
        Debug.Log("productDetail.IAPKEY" + productDetail.IAPKEY);
        if (productDetail.IAPKEY == PurchaseIdentifier.DUALCOIN.ToString())
        {
            PlayerDataManager.Instance.PurchasedDualCoins();
            return;
        }

        for (int i = 0; i < productDetail.productData.Count; i++)
        {
            switch (productDetail.productData[i].currency)
            {
                case Currency.NONE:
                    break;
                case Currency.COINS:
                    PlayerDataManager.Instance.AddCoins(productDetail.productData[i].count);
                    break;
                case Currency.GEMS:
                    PlayerDataManager.Instance.AddGems(productDetail.productData[i].count);
                    break;
                case Currency.GIFT:
                    PlayerDataManager.Instance.AddGifts(productDetail.productData[i].count);
                    break;
                default:
                    break;
            }
        }
    }

    public void OnPurchaseFail(Product product ,  PurchaseFailureReason purchaseFailureReson)
    {
        Debug.Log("Id: " + product.definition.id + "Reason : " + purchaseFailureReson.ToString());
        MessageBoxHandler.Instance.SetupMessageBox("Purchase Unsuccessful", "OOPS!", false, false, false);
        MessageBoxHandler.Instance.Show();
    }

    public void BuyProduct(int index)
    {
        OnPurchaseSuccessFul(index);
    }

    public void InitiatePurchase()
    {

    }
}
