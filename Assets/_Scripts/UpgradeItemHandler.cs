﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeItemHandler : MonoBehaviour
{
    public GameObject levelIndecatorParent;
    public TextMeshProUGUI info;
    public TextMeshProUGUI price;
    public int itemIndex;
    public Currency currency;
    Image[] dotImages;

    private void Awake()
    {
        InitilizeDots();
    }

    private void InitilizeDots()
    {
        if (dotImages == null || dotImages.Length == 0)
        {
            dotImages = levelIndecatorParent.GetComponentsInChildren<Image>(true);
            for (int i = 0; i < dotImages.Length; i++)
            {
                dotImages[i].sprite = UpgradeItemManager.Instance.offSprite;
            }
        }
    }

    public void UpdateUI(UpgradeItemData data)
    {
        InitilizeDots();
        for (int i = 1; i <= data.powerLevel; i++)
        {
            dotImages[i].sprite = UpgradeItemManager.Instance.onSprite;
        }
        if(data.powerLevel >= data.addonTime.Length)
        {
            return;
        }

        //Debug.Log("Power level : " + data.powerLevel);
        int value = data.addonTime[data.powerLevel];
        info.SetText("+" + value + " Seconds");
        price.SetText(data.coinsNeeded[data.powerLevel].ToString());
    }

    public void OnClick()
    {
        //Debug.Log("Start Buy using local currency" + currency);
        UpgradeItemManager.Instance.OnPurchase(itemIndex, currency);
    }

    void UpdateUI()
    {

    }
}
