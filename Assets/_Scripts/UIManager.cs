﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviourSingleton<UIManager>
{
    public GameObject footer;
    public GameObject freeCoins;
    public GameObject headings;
    public GameObject header;
    public GameObject blackBG;
    public GameObject achievementScreen;
    public GameObject adventurescreen;
    public GameObject bankScreen;
    public GameObject upgradeScreen;
    public GameObject boosterScreen;
    public GameObject settingScreen;

    public GameObject playButton;

    public SettingUI settingUI;
    public AdventureUI adventureUI;
    public AchievementUI achievementUI;

    //Commom
    public BlackLayerUI blackLayerUI;
    public BlackLayerUI blackLayerInBoostPopups;


    public SelectionManager selectionManager;

    public Toggle adventureToggle, settingToggle, achivementToggle;
    public GameObject gameSetup, gameUI, gameOverUI, gameOverUIPopup;

    public GameObject mainCanvas;
    public GameObject menuScreen, mainComponants, messagePopup;


    public void Start()
    {
        //InvokeRepeating("AnimatePlay", 0, 4f);
        //mainCanvas.SetActive(true);
        ActivateMainComponants(true);
        gameUI.SetActive(false);
        HideAllScreens();
        GameOverHandler.onSkip -= LaunchMainUI;
        GameOverHandler.onSkip += LaunchMainUI;

        GameOverHandler.onRestart -= OnRestart;
        GameOverHandler.onRestart += OnRestart;
    }

    void OnRestart()
    {
        HideGameOver();
    }

    public void LaunchMainUI()
    {
        HideGameOver();
        ActivateMainComponants(true);
        gameUI.SetActive(false);
        HideAllScreens();
    }

    private void ActivateMainComponants(bool isRequired)
    {
        menuScreen.SetActive(isRequired); mainComponants.SetActive(isRequired);
        //messagePopup.SetActive(isRequired);
    }

    void AnimatePlay()
    {
        LeanTween.scaleX(playButton, 0.7f, 0.2f).setLoopPingPong(1);
        LeanTween.scaleY(playButton, 1.2f, 0.2f).setLoopPingPong(1);
    }

    public void HideAllScreens()
    {
        settingUI.CloseImmidiate();
        achievementUI.CloseImmidiate();
        adventureUI.CloseImmidiate();
    }

    public void OnSettingPressed()
    {
        adventureUI.CloseImmidiate();
        achievementUI.CloseImmidiate();
        settingToggle.isOn = true;
        blackLayerUI.Show();
        settingUI.Show();
    }

    public void OnSettingPressedWithoutLayer()
    {
        adventureUI.CloseImmidiate();
        achievementUI.CloseImmidiate();
        settingUI.Show();
    }

    public void OnAchievementPressed()
    {
        //blackLayerUI.Show();
        settingUI.CloseImmidiate();
        adventureUI.CloseImmidiate();
        achivementToggle.isOn = true;
        achievementUI.Show();
    }

    public void OnAdventurePressed()
    {
        //blackLayerUI.Show();
        settingUI.CloseImmidiate();
        achievementUI.CloseImmidiate();
        adventureToggle.isOn = true;
        adventureUI.Show();
    }

    public void OnSettingClose()
    {
        settingUI.Close();
        blackLayerUI.Close();
    }

    public void OnHomePressed()
    {
        blackLayerUI.Close(() =>
        {
            HideAllScreens();
        });
    }

    public void OnGemsPressed()
    {
        blackLayerUI.Close(() =>
        {
            //Show Coin Purchase Sceen
            HideAllScreens();
            blackLayerInBoostPopups.Show();
            selectionManager.OnBankTabClick();
        });
    }

    public void OnCoinPressed()
    {
        blackLayerUI.Close(() =>
        {
            //Show Coin Purchase Sceen
            HideAllScreens();
            blackLayerInBoostPopups.Show();
            selectionManager.OnBankTabClick(1);
        });
    }

    public void OnAchivemenrPressedFromBoostScreen()
    {
        blackLayerInBoostPopups.Close(() =>
        {
            //Show Coin Purchase Sceen
            blackLayerUI.Show();
            achievementUI.Show();
            achivementToggle.isOn = true;

        });
    }

    public void OnAdventurePressedFromBoostScreen()
    {
        blackLayerInBoostPopups.Close(() =>
        {
            //Show Coin Purchase Sceen
            blackLayerUI.Show();
            adventureUI.Show();
            adventureToggle.isOn = true;

        });
    }

    public void OnHomePressedFromBoostScreen()
    {
        blackLayerInBoostPopups.Close(() =>
        {
            HideAllScreens();
        });
    }

    public void OnPlayPressed()
    {
        HideAllScreens();
        blackLayerInBoostPopups.Show();
        selectionManager.OnHeroesTabClick();
    }

    public void StartGame()
    {
        Debug.LogError("Start Game");
        //mainCanvas.SetActive(false);
        GameManager.Instance.PreparePlayer();
        PlatformGenerator.Instance.Initilize();
        ActivateMainComponants(false);
        gameSetup.SetActive(true);
        gameUI.SetActive(true);
        CountDown.Instance.StartCountDown();
    }

    public void ShowGameOver()
    {
        gameOverUI.SetActive(true);
        LeanTween.moveY(gameOverUIPopup.GetComponent<RectTransform>(), 0, 0.6f).setEase(LeanTweenType.easeOutBack);
    }

    public void HideGameOver()
    {
        gameOverUI.SetActive(false);
    }
}
