﻿using System;
using UnityEngine;

public class BlackLayerUI : MonoBehaviour
{
    bool isOpen = false;

    void OnEnable()
    {
        isOpen = true;
    }

    public void Show()
    {
        LeanTween.scale(gameObject, Vector3.zero, 0f);
        gameObject.SetActive(true);
        LeanTween.scale(gameObject, Vector3.one, 0.35f).setEase(LeanTweenType.easeOutBack);
    }

    public void OnBlackLayerPresses()
    {
        UIManager.Instance.OnSettingClose();
    }

    public void Close(Action onClose = null)
    {
        LeanTween.scale(gameObject, Vector3.zero, 0.35f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            gameObject.SetActive(false);
            if(onClose!=null)
            {
                onClose();
            }
        });
    }
}
