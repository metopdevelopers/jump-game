﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AchievementData
{
    public string objective;
    public string task;
    public Currency awardCurrency;
    public int count;
}

[System.Serializable]
public class AchievementDetail
{
    public List<AchievementData> achivementDatas;
}

public class AchivementHandler : MonoBehaviourSingleton<AchivementHandler>
{
    AchievementDetail achievementDetail = null;
    public const string ACHIEVEMENT_KEY = "ACHIEVEMENT";

    void Start()
    {
        achievementDetail = JsonUtility.FromJson<AchievementDetail>(PlayerPrefs.GetString(ACHIEVEMENT_KEY, null));
        if (achievementDetail == null || (achievementDetail != null && achievementDetail.achivementDatas != null && achievementDetail.achivementDatas.Count == 0))
        {
            achievementDetail = new AchievementDetail();
            SetupAchievementDetail();
        }
    }

    private void SetupAchievementDetail()
    {
        achievementDetail.achivementDatas = new List<AchievementData>();
        achievementDetail.achivementDatas = GetProductDetails();
        string iapJSON = JsonUtility.ToJson(achievementDetail);
        PlayerPrefs.SetString(ACHIEVEMENT_KEY, iapJSON);
    }

    public AchievementDetail GetAchivementDetail()
    {
        return achievementDetail;
    }

    private List<AchievementData> GetProductDetails()
    {
        List<AchievementData> achievementData = new List<AchievementData>();

        AchievementData data = new AchievementData();
        data.objective = "Coin Hoarder";
        data.task = "Collect 500 Coins Total";
        data.awardCurrency = Currency.GEMS;
        data.count = 1;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Gem Hoarder";
        data.task = "Collect 50 Gems Total";
        data.awardCurrency = Currency.COINS;
        data.count = 500;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Free Coins";
        data.task = "Watch a Video ad for Coins";
        data.awardCurrency = Currency.GEMS;
        data.count = 2;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Unlimited Power";
        data.task = "Use all Artifacts in a single game";
        data.awardCurrency = Currency.COINS;
        data.count = 1000;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "I Can Go The Distance";
        data.task = "Travel 1,000,000M Total";
        data.awardCurrency = Currency.GEMS;
        data.count = 20;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Power-up";
        data.task = "Enhance an Upgrade 4 times";
        data.awardCurrency = Currency.COINS;
        data.count = 2000;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Purchase an Artifact";
        data.task = "Buy one Artifact";
        data.awardCurrency = Currency.COINS;
        data.count = 500;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Buy an Upgrade";
        data.task = "Buy one Upgrade";
        data.awardCurrency = Currency.COINS;
        data.count = 500;
        achievementData.Add(data);


        data = new AchievementData();
        data.objective = "Full Party";
        data.task = "Unlock all Heroes";
        data.awardCurrency = Currency.COINS;
        data.count = 10000;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Never Give Up!";
        data.task = "Play 50 Games";
        data.awardCurrency = Currency.COINS;
        data.count = 1000;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Maximum Power";
        data.task = "Max out a single Upgrade";
        data.awardCurrency = Currency.GEMS;
        data.count = 30;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Fortunas Thanks";
        data.task = "Open 100 Fortuna Mystery Boxes";
        data.awardCurrency = Currency.GEMS;
        data.count = 5;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Baby Points";
        data.task = "Score 1000 Points in a single game";
        data.awardCurrency = Currency.COINS;
        data.count = 500;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "High Score";
        data.task = "Score 6000 Points in a single game";
        data.awardCurrency = Currency.COINS;
        data.count = 1000;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Secrets x1";
        data.task = "Unlock a secret Hero";
        data.awardCurrency = Currency.COINS;
        data.count = 2000;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Secrets x2";
        data.task = "Unlock two secret Heroes";
        data.awardCurrency = Currency.COINS;
        data.count = 3000;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Secrets x3";
        data.task = "Unlock three secret Heroes";
        data.awardCurrency = Currency.COINS;
        data.count = 4000;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Master of Secrets";
        data.task = "Unlock all secret Heroes";
        data.awardCurrency = Currency.COINS;
        data.count = 8000;
        achievementData.Add(data);

        data = new AchievementData();
        data.objective = "Revive";
        data.task = "Revive";
        data.awardCurrency = Currency.COINS;
        data.count = 1000;
        achievementData.Add(data);

        return achievementData;
    }


}
