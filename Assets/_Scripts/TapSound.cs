﻿using UnityEngine;
using UnityEngine.UI;

public class TapSound : MonoBehaviour
{
    Button button;
    void OnEnable()
    {
        if (button == null)
        {
            button = GetComponent<Button>();
            if (button != null)
            {
                button.onClick.AddListener(PlayTapSound);
            }
        }
    }

    public void PlayTapSound()
    {
        SoundManager.Instance.playSFX(3);
    }
}
