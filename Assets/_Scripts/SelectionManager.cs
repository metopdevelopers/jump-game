﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SelectionManager : MonoBehaviourSingleton<SelectionManager>
{
    public BankUI bankUI;
    public UpgradesUI upgradeUI;
    public HeroesUI heroesUI;
    public BoostUI boostUI;

    public Toggle boost, heroes, upgrades, bank;
    public Toggle adventure, achivement;


    public TextMeshProUGUI header;

    public void HideAllUIElements(int index)
    {
        //1:Hero, 2:Boost, 3:Upgrade, 4:Bank
        adventure.isOn = false;
        achivement.isOn = false;
        switch (index)
        {
            case 1:
                boostUI.CloseImmidiate();
                upgradeUI.CloseImmidiate();
                bankUI.CloseImmidiate();
                heroes.isOn = true;
                break;

            case 2:
                heroesUI.CloseImmidiate();
                upgradeUI.CloseImmidiate();
                bankUI.CloseImmidiate();
                boost.isOn = true;
                break;

            case 3:
                heroesUI.CloseImmidiate();
                boostUI.CloseImmidiate();
                bankUI.CloseImmidiate();
                upgrades.isOn = true;
                break;

            case 4:
                heroesUI.CloseImmidiate();
                boostUI.CloseImmidiate();
                upgradeUI.CloseImmidiate();
                bank.isOn = true;
                break;
            
            default:
                break;
        }
    }

    public void OnBoostTabClick ()
    {
        HideAllUIElements(2);
        header.SetText("ARTIFACTS");
        LeanTween.delayedCall(0.1f , ()=>{
            boostUI.Show();
        });
    }

    public void OnUpgradeTabClick()
    {
        HideAllUIElements(3);
        header.SetText("UPGRADES");
        LeanTween.delayedCall(0.1f, () => {
            upgradeUI.Show();
        });
    }

    public void OnHeroesTabClick()
    {
        HideAllUIElements(1);
        header.SetText("HEROES");
        LeanTween.delayedCall(0.1f, () => {
            heroesUI.Show();
        });
    }

    public void OnBankTabClick(int index = 0)
    {
        Debug.Log("Bank tab click0");
        HideAllUIElements(4);
        header.SetText("BANK");
        BankUI.needCoinsScreen = index == 1;
        LeanTween.delayedCall(0.1f, () => {
            bankUI.Show();
        });
    }
}
