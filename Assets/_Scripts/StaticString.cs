﻿public class StaticString
{
    public const string WATCH_VIDEO_HEADER = "Earn REWARD";
    public const string WATCH_VIDEO_INFO = "Would you like to watch a video to Recieve 100 coins for free";
    public static string[] UPGRADE_INFO = { 
        "Temporarily double your score multiplier!", 
        "Become giant and squish enemies", "With fire thump,you can't fall and blast enemies!",
        "With fireballl active, you can't fall and can blast enemeis!",
        "Make coins and tokens come to you for a change!",
        "Become invincible as you collect tons of coins!", 
        "Freeze all moving obstacles! shatter frozen enemies!", 
        "Allow you to jump on fake platforms for the duration!",
        "Sloooow doooown for a little bit!" 
    };
    public static string[] UPGRADE_HEADER = { "DOUBLE SCORE", "COLOSSOL", "FIRE THUMP", "MEGNETISM", "ATTACK MODE ", "STOP DEAD", "TRICKY JUMP", "SLOW FLOW" };
    public static string[] CHARACTER_NAMES = { "ZEUS", "HERA", "POSEIDON", "ATHENA", "APOLLO", "MEDUSA", "CERBERUS", "CENTAUR" , "MINOTOUR" , "ARES" , "CYCLOPS" , "HERCULES" , "ARTEMIS" , "PEGASUS" , "APHRODITE" , "HADES" };

    public static string[] BOOST_DETAILS = { "", "Tap the lucky prize to look inside", "Blocks one hit from a baddie for 20 seconds", "Add 5 to your score multiplier for one game",
                                            "Boost 250m at the start of the game!", "Add 5 to your score multiplier for one game!", "Buy a Boost", "Every coins becomes worth double! Lasts forever" };

    public static string[] BOOST_HEADER = { "APOLLO STARTER KIT", "FORTUNA MYSTERY BOX", "AEGIS SHIELD", "SCORE BOOST", "HEAD START" , "SKIP ADVENTURE 2" , "SKIP ADVENTURE 3" , "DUAL COINS"};
}