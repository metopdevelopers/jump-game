﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
    public TextMeshProUGUI coins, gems;

    private void OnEnable()
    {
        PlayerDataManager.OnGemsUpdate-= OnGemsUpdate;
        PlayerDataManager.OnGemsUpdate += OnGemsUpdate;


        PlayerDataManager.OnCoinUpdate -= OnCoinUpdate;
        PlayerDataManager.OnCoinUpdate += OnCoinUpdate;

        OnCoinUpdate(PlayerDataManager.Instance.GetCoinCount());
        OnGemsUpdate(PlayerDataManager.Instance.GetGemsCount());

    }

    private void OnCoinUpdate(int coinCount)
    {
        coins.SetText(coinCount.ToString());
    }

    private void OnGemsUpdate(int gemCount)
    {
        gems.SetText(gemCount.ToString());
    }

    private void OnDisable()
    {
        PlayerDataManager.OnGemsUpdate -= OnGemsUpdate;
        PlayerDataManager.OnGemsUpdate -= OnCoinUpdate;
    }
}
