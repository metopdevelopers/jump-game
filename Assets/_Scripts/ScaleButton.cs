﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ScaleButton : MonoBehaviour
{
    Button button;
    private void Start()
    {
        button = GetComponent<Button>();
        if(button!=null)
        {
            button.onClick.AddListener(OnClick);
        }
    }

    void OnClick()
    {
        Debug.LogError("On click");
        button.enabled = false;
        //SoundManager.Instance.playSFX(3);
        LeanTween.scale(gameObject, Vector3.one * 1.2f, 0.08f).setOnComplete(()=>{
            LeanTween.scale(gameObject, Vector3.one , 0.08f).setOnComplete(() => {
                button.enabled = true;
            });
        });
    }
}
