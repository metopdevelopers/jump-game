﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

[System.Serializable]
public class MissionItem
{
    public GameObject purchaseButton;
    public GameObject completeCheckMark;
    public TextMeshProUGUI price, task;
}

public class AdventureUI : MonoBehaviour
{
    bool isOpen = false;
    public List<MissionItem> missionItems;
    public TextMeshProUGUI scoreMultiplier;
    public int currentScoreMultiplier;

    void OnEnable()
    {
        isOpen = true;
    }

    public void Show()
    {
        if (isOpen)
        {
            return;
        }
        gameObject.SetActive(true);
        LeanTween.scale(gameObject, Vector3.zero, 0f);
        LeanTween.scale(gameObject, Vector3.one, 0.35f).setEase(LeanTweenType.easeOutBack);
        SetupUI();
    }

    public void Close()
    {
        LeanTween.scale(gameObject, Vector3.zero, 0.35f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            gameObject.SetActive(false);
        });
    }

    public void CloseImmidiate()
    {
        LeanTween.scale(gameObject, Vector3.zero, 0f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            gameObject.SetActive(false);
        });
    }

    public void SetupUI()
    {
        string[] tasks = MissionDataManager.Instance.ActiveMission.GetSubMissions();
        int[] prices = MissionDataManager.Instance.ActiveMission.GetPurchaseAmounts();
        bool[] achievementStatus = MissionDataManager.Instance.ActiveMission.GetAchievementStatus();
        for (int i = 0; i < tasks.Length; i++)
        {
            missionItems[i].task.SetText(tasks[i]);
            missionItems[i].price.SetText(prices[i].ToString());
            missionItems[i].completeCheckMark.SetActive(achievementStatus[i]);
            missionItems[i].purchaseButton.SetActive(!achievementStatus[i]);
        }
        UpdateMultiplier();
    }

    private void UpdateMultiplier()
    {
        scoreMultiplier.SetText("SCORE X" + (MissionDataManager.Instance.GetCurrentMultiplier()).ToString());
    }

    public void SkipMission(int missionIndex)
    {
        bool[] achievementStatus = MissionDataManager.Instance.ActiveMission.GetAchievementStatus();
        int[] prices = MissionDataManager.Instance.ActiveMission.GetPurchaseAmounts();
        if (missionIndex > achievementStatus.Length)
            return;

        if (!achievementStatus[missionIndex] && CanPurchase(prices[missionIndex]))
        {
            achievementStatus[missionIndex] = true;
            missionItems[missionIndex].completeCheckMark.SetActive(true);
            missionItems[missionIndex].purchaseButton.SetActive(false);
        }



        if (IsAllTaskOfCurrentMissioncompleted())
        {
            MissionDataManager.Instance.OnMissionAchieved();
            MoveToNextMission();
            UpdateMultiplier();
        }
    }

    bool CanPurchase(int requiredCoins)
    {
        if (PlayerDataManager.Instance.GetCoinCount() > 0 && PlayerDataManager.Instance.GetCoinCount() >= requiredCoins)
        {
            //User can but the item
            PlayerDataManager.Instance.AddCoins(-requiredCoins);
            return true;
        }
        else
        {
            ShowAlert();
            return false;
        }
    }

    bool IsAllTaskOfCurrentMissioncompleted()
    {
        bool status = true;
        bool[] achievementStatus = MissionDataManager.Instance.ActiveMission.GetAchievementStatus();
        for (int i = 0; i < achievementStatus.Length; i++)
        {
            if (!achievementStatus[i])
            {
                status = false;
                break;
            }
        }
        return status;
    }

    public void MoveToNextMission()
    {
        SetupUI();
    }



    private static void ShowAlert()
    {
        MessageBoxHandler.Instance.SetupMessageBox("You need more Coins. Play to collect coins", "OOPS!", false, false, false, null, null, null);
        MessageBoxHandler.Instance.Show();
    }
}

