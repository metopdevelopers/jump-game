﻿using System;
using UnityEngine;

public class ScoreManager : MonoBehaviourSingleton<ScoreManager>
{
    public const string SCORE_KEY = "SCORE";
    private Score scoreData;
    public static Action<int> onScoreUpdate = delegate { };
    private void Awake()
    {
        string _scoreData = PlayerPrefs.GetString(SCORE_KEY, JsonUtility.ToJson(new Score()));
        scoreData = JsonUtility.FromJson<Score>(_scoreData);
    }

    public void UpdateGems(int score)
    {
        scoreData.currentGemScore += score;
        onScoreUpdate(score);
    }

    public void UpdateCoins(int score)
    {
        scoreData.currentCoinsScore += score;
        onScoreUpdate(score);
    }

    public int GemsScore()
    {
        return scoreData.currentGemScore;
    }

    public int CoinsScore()
    {
        return scoreData.currentCoinsScore;
    }

    public void SaveData()
    {
        PlayerPrefs.SetString(SCORE_KEY, JsonUtility.ToJson(scoreData));
        Debug.Log("Score data saved");
    }
}
