﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
public enum BoosterType
{
    DFENCE,
    BOOSTSCORE,
    BOOSTUP
}

[System.Serializable]
public class BoosterItemData
{
    public BoosterType boosterType;
    public int boosterCount;
    public bool isRealMoneyProduct;
    public int coinsRequired;
}

public class BoosterItem : MonoBehaviour
{
    public BoosterItemData boosterItem;
    public TextMeshProUGUI count , priceText;
    private int itemIndex;
    public void SetupData(BoosterItemData _boosterItem , int itemIndex)
    {
        this.boosterItem = _boosterItem;
        count.SetText(boosterItem.boosterCount.ToString());
        priceText.SetText(boosterItem.coinsRequired.ToString());
        this.itemIndex = itemIndex;
    }

    public void OnClick()
    {
        BoosterManager.Instance.OnPurchase(itemIndex, Currency.COINS, (bool isPurchased , int updatedCount) =>
        {
            if(isPurchased)
            {
                count.SetText(updatedCount.ToString());
            }
        });
    }
}
