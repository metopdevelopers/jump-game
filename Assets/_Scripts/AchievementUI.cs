﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AchievementUI : MonoBehaviour
{
    public GameObject achivementPrefab;
    public Transform parentTransform;
    public Sprite Gem, Coin;
    bool isOpen = false;
    public bool isInitilized = false;
    void OnEnable()
    {
        isOpen = true;
    }

    public void Show()
    {
        if (isOpen)
        {
            return;
        }
        gameObject.SetActive(true);
        LeanTween.scale(gameObject, Vector3.zero, 0f);
        LeanTween.scale(gameObject, Vector3.one, 0.35f).setEase(LeanTweenType.easeOutBack);
        SetupUI();
    }

    public void Close()
    {
        LeanTween.scale(gameObject, Vector3.zero, 0.35f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            gameObject.SetActive(false);
        });
    }

    public void CloseImmidiate()
    {
        LeanTween.scale(gameObject, Vector3.zero, 0f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            gameObject.SetActive(false);
        });
    }

    public void SetupUI()
    {
        if (isInitilized)
            return;

        AchievementDetail achievementDetails = AchivementHandler.Instance.GetAchivementDetail();
        foreach (var item in achievementDetails.achivementDatas)
        {
            GameObject go = Instantiate(achivementPrefab) as GameObject;
            go.transform.SetParent(parentTransform);
            go.transform.localScale = Vector3.one;
            Achievement achievement = go.GetComponent<Achievement>();
            achievement.description.SetText(item.task);
            achievement.heading.SetText(item.objective);
            achievement.price.SetText(item.count.ToString());
            achievement.percentage.SetText(Random.Range(10 , 100)+ "%");
            achievement.icon.sprite = item.awardCurrency == Currency.GEMS ? Gem : Coin;
        }
        isInitilized = true;
    }
}

