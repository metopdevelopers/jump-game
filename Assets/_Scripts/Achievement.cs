﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Achievement : MonoBehaviour
{
    public TextMeshProUGUI heading, description, price , percentage;
    public Image icon;
}