﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class HeroesUI : MonoBehaviour
{
    bool isOpen = false;
    public HorizontalScrollSnap horizontalScrollSnap;
    public TextMeshProUGUI heroName;
    public GameObject unlockedGO, includedGO, selectGO;

    public TextMeshProUGUI[] characterBenefits;
    public TextMeshProUGUI price;
    public Image icon;

    public Sprite coin, diamond;
    int purchaseItemIndex;
    void OnEnable()
    {
        isOpen = true;
        heroName.SetText(StaticString.CHARACTER_NAMES[0]);
        HorizontalScrollSnap.OnMovementStart -= UpdateUI;
        HorizontalScrollSnap.OnMovementStart += UpdateUI;

    }

    private void UpdateUI(int currentPlayerIndex)
    {
        //Debug.Log("Updating name : " + currentPlayerIndex);
        purchaseItemIndex = currentPlayerIndex;
        includedGO.SetActive(false);
        selectGO.SetActive(false);
        unlockedGO.SetActive(false);

        CharacterDataSet characterDataSet = CharacterData.Instance.GetCharacterData();
        for (int i = 0; i < characterBenefits.Length; i++)
        {
            characterBenefits[i].gameObject.SetActive(false);
        }

        if (characterDataSet == null)
        {
            return;
        }
        GameCharacter gameCharacter = characterDataSet.GameCharacters[currentPlayerIndex];
        for (int i = 0; i < gameCharacter.benefits.Length; i++)
        {
            characterBenefits[i].gameObject.SetActive(true);
            characterBenefits[i].SetText(gameCharacter.benefits[i]);
        }

        switch (gameCharacter.purchaseStatus)
        {
            case PurchaseStatus.NOTPURCHASED:
                unlockedGO.SetActive(false);
                break;
            case PurchaseStatus.PURCHASED:
                if (gameCharacter.isSelected)
                {
                    includedGO.SetActive(true);
                }
                else
                {
                    selectGO.SetActive(true);
                }
                break;
            case PurchaseStatus.UNLOCABLE:
                unlockedGO.SetActive(true);
                icon.gameObject.SetActive(true);
                icon.sprite = gameCharacter.currency == Currency.COINS ? coin : diamond;
                price.SetText(gameCharacter.amountRequiredToPurchase.ToString());
                break;
            case PurchaseStatus.NONE:
                unlockedGO.SetActive(true);
                if (gameCharacter.unlockedLevel > 0)
                {
                    icon.gameObject.SetActive(false);
                    price.SetText(gameCharacter.unlockedLevel + " Shards");
                }
                break;
            default:

                break;
        }

        heroName.SetText(StaticString.CHARACTER_NAMES[currentPlayerIndex]);
    }

    public void Show()
    {
        if (isOpen)
        {
            return;
        }
        gameObject.SetActive(true);
        //LeanTween.scale(gameObject, Vector3.zero, 0f);
        //LeanTween.scale(gameObject, Vector3.one, 0.35f).setEase(LeanTweenType.easeOutBack).setOnComplete(() => {
        //    horizontalScrollSnap.Reset();
        //});
    }

    public void Close()
    {
        LeanTween.scale(gameObject, Vector3.zero, 0.35f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            gameObject.SetActive(false);
        });
    }

    public void CloseImmidiate()
    {
        //LeanTween.scale(gameObject, Vector3.zero, 0f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        //{
        //    isOpen = false;
        //    gameObject.SetActive(false);
        //});
        isOpen = false;
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        isOpen = false;
    }

    public void SelectCharacter()
    {
        CharacterDataSet characterDataSet = CharacterData.Instance.GetCharacterData();
        characterDataSet.SelectCharacter(purchaseItemIndex);
        CharacterData.Instance.Save();
        UpdateUI(purchaseItemIndex);
    }

    public void PurchaseItem()
    {
        CharacterDataSet characterDataSet = CharacterData.Instance.GetCharacterData();
        GameCharacter gameCharacter = characterDataSet.GameCharacters[purchaseItemIndex];

        if (gameCharacter.currency == Currency.COINS)
        {
            if (PlayerDataManager.Instance.GetCoinCount() > 0 && PlayerDataManager.Instance.GetCoinCount() >= gameCharacter.amountRequiredToPurchase)
            {
                //User can but the item
                PlayerDataManager.Instance.AddCoins(-gameCharacter.amountRequiredToPurchase);
                characterDataSet.SetCharacterPurchased(purchaseItemIndex);
                CharacterData.Instance.Save();
                UpdateUI(purchaseItemIndex);
            }
            else
            {
                ShowAlert(1);
            }
        }
        else if (gameCharacter.currency == Currency.GEMS)
        {
            if (PlayerDataManager.Instance.GetGemsCount() > 0 && PlayerDataManager.Instance.GetGemsCount() >= gameCharacter.amountRequiredToPurchase)
            {
                //User can but the item
                PlayerDataManager.Instance.AddGems(-gameCharacter.amountRequiredToPurchase);
                characterDataSet.SetCharacterPurchased(purchaseItemIndex);
                CharacterData.Instance.Save();
                UpdateUI(purchaseItemIndex);
            }
            else
            {
                ShowAlert(0);
            }
        }
    }

    private static void ShowAlert(int index)
    {
        MessageBoxHandler.Instance.SetupMessageBox("You need more Coins.  Play to collect coins, or get more from the bank.", "OOPS!", false, false, true, () =>
        {
            Debug.Log("Will open coin shop");
            SelectionManager.Instance.OnBankTabClick(index);
        }, null, null, "BANK");
        MessageBoxHandler.Instance.Show();
    }
}


