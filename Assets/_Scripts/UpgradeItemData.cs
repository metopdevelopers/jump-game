﻿using System.Collections.Generic;

[System.Serializable]
public class UpgradeItemData
{
    public int powerLevel;
    public int[] addonTime;
    public int[] coinsNeeded;

    public int GetCoinsNeeded()
    {
        return coinsNeeded[powerLevel];
    }
}
[System.Serializable]
public class UpgradeItemsData
{
    public List<UpgradeItemData> updgradeItemsData;
}
