﻿public enum TileType
{
    NORMAL,
    BREAKABLE_TILE
}

public interface ITile
{
    void SetTileType(TileType tileType);
    void PerformAction();
}
