﻿using UnityEngine;

public class BreakableTile : MonoBehaviour, ITile
{
    public Animator tileAnimation;
    public void PerformAction()
    {
        //Debug.Log("Breake the tile animation");
        GameManager.Instance.player.jump(1.15f);
        tileAnimation.SetBool("Start" , true);
    }

    public void SetTileType(TileType tileType)
    {
    }

    public void OnAniamtionEnd()
    {
        Destroy(gameObject);
    }
}
