﻿using UnityEngine;

public class MoveableTiles : MonoBehaviour, ITile
{
    Vector2 dir = Vector2.right;
    public float speed = 5 , min , max;
    bool start = false;
    public void StartMovememt(float min , float max)
    {
        start = true;
        this.min = min;
        this.max = max;
    }

    private void Update()
    {
        if (!start && !GameManager.Instance.readyForJump)
            return;

        transform.Translate(dir * speed * Time.deltaTime);

        if (transform.position.x <= min)
        {
            dir = Vector3.right;
        }
        else if (transform.position.x >= max)
        {
            dir = Vector3.left;
        }
    }

    public void PerformAction()
    {
        //Debug.LogError("Player can jump");
        GameManager.Instance.player.jump(1.15f);
    }

    public void SetTileType(TileType tileType)
    {

    }
}