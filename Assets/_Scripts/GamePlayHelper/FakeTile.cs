﻿using UnityEngine;

public class FakeTile : MonoBehaviour, ITile
{
    public Animator tileAnimation;
    public void PerformAction()
    {
        //GameManager.Instance.player.jump(1);
        tileAnimation.SetBool("Start", true);
        //Debug.LogError("Destroy it after animation");
    }

    public void SetTileType(TileType tileType)
    {
    }

    public void OnAniamtionEnd()
    {
        Destroy(gameObject);
    }
}
