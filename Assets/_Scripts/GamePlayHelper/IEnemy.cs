﻿public enum EnemyType
{
    PINK,
    BLUE,
    ORANGE
}

public interface IEnemy
{
    void SetTileType(EnemyType enemyType);
    void PerformAction();
}
