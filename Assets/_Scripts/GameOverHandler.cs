﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameOverHandler : MonoBehaviourSingleton<GameOverHandler>
{
    public static Action onSkip = delegate { };
    public static Action onRestart = delegate { };
    public Image player;
    public GameObject scoreScreen;
    public TextMeshProUGUI gemsScore, coinsScore;
    public void OnEnable()
    {
        player.sprite = CharacterData.Instance.GetPlayerSprite();
        gemsScore.SetText(ScoreManager.Instance.GemsScore().ToString());
        coinsScore.SetText(ScoreManager.Instance.CoinsScore().ToString());
    }

    public void ShowScore()
    {

    }

    public void OnSkip()
    {
        Debug.Log("On Skip");
        ScoreManager.Instance.SaveData();
        UIManager.Instance.HideGameOver();
        scoreScreen.SetActive(true);
        LeanTween.scale(scoreScreen, Vector3.zero, 0f);
        LeanTween.scale(scoreScreen, Vector3.one, 0.35f).setEase(LeanTweenType.easeOutBack);
    }

    public void OnRestart()
    {
        Debug.Log("On Restart");
        onRestart();
    }

    public void onOkPressed()
    {
        LeanTween.scale(scoreScreen, Vector3.zero, 0.20f).setEase(LeanTweenType.easeOutBack).setOnComplete(()=>{
            scoreScreen.SetActive(false);
        });
        onSkip();
    }
}
