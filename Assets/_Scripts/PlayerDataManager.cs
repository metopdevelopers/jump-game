﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int coins;
    public int gems;
    public int gifts;
    public bool isDualCoinPurchased;
}

public class PlayerDataManager : MonoBehaviourSingleton<PlayerDataManager>
{
    public const string PLAYERDATAKEY = "PlayerData";
    PlayerData playerData = null;

    public static Action<int> OnCoinUpdate;
    public static Action<int> OnGemsUpdate;

   

    private void Start()
    {
        playerData = JsonUtility.FromJson<PlayerData>(PlayerPrefs.GetString(PLAYERDATAKEY, null));
        if (playerData == null)
        {
            playerData = new PlayerData();
            AddCoins(50);
            AddGems(5);
        }
    }

    public void AddCoins(int coinsCount)
    {
        Debug.Log("Adding Coins" + coinsCount);

        int updatedCount = playerData.coins + coinsCount;
        int previosCount = playerData.coins;
        playerData.coins = updatedCount;
        LeanTween.value(previosCount, updatedCount, 0.5f).setOnUpdate((float updatedValue) =>
        {
            //coins.SetText(((int)updatedCount).ToString());
            OnCoinUpdate?.Invoke((int)updatedValue);
        });
        Save();
    }

    private void Save()
    {
        PlayerPrefs.SetString(PLAYERDATAKEY, JsonUtility.ToJson(playerData));
    }

    protected internal void AddGems(int gemsCount)
    {
        Debug.Log("Adding Gems" + gemsCount);
        int updatedCount = playerData.gems + gemsCount;
        int previosCount = playerData.gems;
        playerData.gems = updatedCount;
        LeanTween.value(previosCount, updatedCount, 0.5f).setOnUpdate((float updatedValue) =>
        {
            OnGemsUpdate?.Invoke((int)updatedValue);
            //gems.SetText(((int)updatedCount).ToString());
        });
        Save();
    }

    public void AddGifts(int giftCount)
    {
        Debug.Log("Adding Gift" + giftCount);
        playerData.gifts += giftCount;
        Debug.Log("Adding Gift" + playerData.gifts);
        Save();
    }

    public void PurchasedDualCoins()
    {
        playerData.isDualCoinPurchased = true;
        Save();
    }

    public bool GetDualcoinPurchaseStatus()
    {
        return playerData.isDualCoinPurchased;
    }

    public int GetCoinCount()
    {
        return playerData.coins;
    }

    public int GetGemsCount()
    {
        return playerData.gems;
    }
}
