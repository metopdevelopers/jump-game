﻿using UnityEngine;
using UnityEngine.UI.Extensions;

public class UpgradesUI : MonoBehaviour
{
    public HorizontalScrollSnap horizontalScrollSnap;
    bool isOpen = false;
    void OnEnable()
    {
        isOpen = true;
    }

    public void Show()
    {
        if (isOpen)
        {
            return;
        }
        gameObject.SetActive(true);
        //LeanTween.scale(gameObject, Vector3.zero, 0f);
        //LeanTween.scale(gameObject, Vector3.one, 0.35f).setEase(LeanTweenType.easeOutBack).setOnComplete(() => {
        //    //horizontalScrollSnap.Reset();
        //}); 
    }

    public void Close()
    {
        LeanTween.scale(gameObject, Vector3.zero, 0.35f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            gameObject.SetActive(false);
        });
    }

    public void CloseImmidiate()
    {
        //LeanTween.scale(gameObject, Vector3.zero, 0f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        //{
        //    isOpen = false;
        //    gameObject.SetActive(false);
        //});
        isOpen = false;
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        isOpen = false;
    }

    public void OnInfoClicked(int index)
    {
        MessageBoxHandler.Instance.SetupMessageBox(StaticString.UPGRADE_INFO[index], StaticString.UPGRADE_HEADER[index], false, false, false);
    }
}


