﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountDown : MonoBehaviourSingleton<CountDown>
{
    public GameObject ready, go;

    public static Action readyGoEnd = delegate { };

    public void StartCountDown()
    {
        CanvasGroup readyCanvasGroup = ready.GetComponent<CanvasGroup>();
        CanvasGroup goCanvasGroup = go.GetComponent<CanvasGroup>();
        LeanTween.alphaCanvas(goCanvasGroup, 1, 0f);
        LeanTween.alphaCanvas(readyCanvasGroup, 1, 0f);
        GameManager.Instance.startControl = true;
        ready.SetActive(true);
        go.SetActive(true);
        go.transform.localScale = ready.transform.localScale = Vector3.zero;

        LeanTween.scale(ready, Vector3.one, 1f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            LeanTween.alphaCanvas(readyCanvasGroup, 0, 0.6f);
            LeanTween.scale(ready, Vector3.one * 2, 0.6f).setOnComplete(() =>
            {
                LeanTween.scale(go, Vector3.one, 1f).setOnComplete(() =>
                {
                    LeanTween.alphaCanvas(goCanvasGroup, 0, 0.6f);
                    LeanTween.scale(go, Vector3.one * 2 , 0.6f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
                    {
                        ready.SetActive(false);
                        go.SetActive(false);
                        readyGoEnd();
                    });
                });
            });
        });
    }
}

