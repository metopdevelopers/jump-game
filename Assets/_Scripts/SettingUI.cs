﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum Controls
{
    ONEHANDED,
    TWOHANDED,
    TILT
}

[System.Serializable]
public class Setting
{
    public bool Music;
    public bool Sound;
    public bool Vibration;
    public Controls Controls = Controls.ONEHANDED;
}

public class SettingUI : MonoBehaviour
{
    public const string SETTING_KEY = "SETTING";
    bool isOpen = false;
    public Toggle setting;
    public Setting settingData;
    public Toggle music, sound, vibration;
    public Toggle controlOneHanded, controlTwoFingers, controlTilt;
    public static Action<bool> onMusicPreferenceChange = delegate { }, onSoundPreferenceChange = delegate { };

    void OnEnable()
    {
        isOpen = true;
        string settingJson = PlayerPrefs.GetString(SETTING_KEY, JsonUtility.ToJson(new Setting()));
        settingData = JsonUtility.FromJson<Setting>(settingJson);
        SetupDetails();
    }

    public void Show()
    {
        if (isOpen)
        {
            return;
        }
        setting.isOn = true;
        gameObject.SetActive(true);
        LeanTween.scale(gameObject, Vector3.zero, 0f);
        LeanTween.scale(gameObject, Vector3.one, 0.35f).setEase(LeanTweenType.easeOutBack);
    }

    public void Close()
    {
        if (isOpen)
        {
            SaveData();
        }
        LeanTween.scale(gameObject, Vector3.zero, 0.35f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            gameObject.SetActive(false);
        });
    }

    public void CloseImmidiate()
    {
        if (isOpen)
        {
            SaveData();
        }
        LeanTween.scale(gameObject, Vector3.zero, 0f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            isOpen = false;
            gameObject.SetActive(false);
        });
    }

    void SetupDetails()
    {
        music.isOn = settingData.Music;
        sound.isOn = settingData.Sound;
        vibration.isOn = settingData.Vibration;
        controlTilt.isOn = (Controls)settingData.Controls == Controls.TILT;
        controlOneHanded.isOn = (Controls)settingData.Controls == Controls.ONEHANDED;
        controlTwoFingers.isOn = (Controls)settingData.Controls == Controls.TWOHANDED;      

    }

    public void OnMusicClick()
    {
        settingData.Music = !settingData.Music;
        music.isOn = settingData.Music;
        onMusicPreferenceChange(settingData.Music);
        SaveData();
    }

    public void OnSoundClick()
    {
        settingData.Sound = !settingData.Sound;
        sound.isOn = settingData.Sound;
        onSoundPreferenceChange(settingData.Sound);
        SaveData();
    }
    public void OnVibrationClick()
    {

    }

    public void OnCreditClick()
    {
        MessageBoxHandler.Instance.SetupMessageBox("Coming Soon", "OOPS!", false, false, false);
        MessageBoxHandler.Instance.Show();
    }

    public void OnSupportClick()
    {
        MessageBoxHandler.Instance.SetupMessageBox("Coming Soon", "OOPS!", false, false, false);
        MessageBoxHandler.Instance.Show();
    }

    public void OnTiltControlClick()
    {
        settingData.Controls = Controls.TILT;
    }

    public void OnTwoHandedControlClick()
    {
        settingData.Controls = Controls.TWOHANDED;
    }

    public void OnOneHandedControlClick()
    {
        settingData.Controls = Controls.ONEHANDED;
    }

    public void SaveData()
    {
        settingData.Music = music.isOn;
        settingData.Sound = sound.isOn;
        settingData.Vibration = vibration.isOn;
        PlayerPrefs.SetString(SETTING_KEY, JsonUtility.ToJson(settingData));
        Debug.Log("Setting data saved");
    }
}
