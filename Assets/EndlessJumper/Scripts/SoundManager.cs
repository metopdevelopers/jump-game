﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviourSingleton<SoundManager> {

	//plays sfx - SFX provided by Freesfx.co.uk (Free)
	public AudioClip[] sfx;
    public AudioSource musicSource;
    // Use this for initialization
    public const string SETTING_KEY = "SETTING";
    void OnEnable () {
        SettingUI.onMusicPreferenceChange -= OnMusicPreferenceChange;
        SettingUI.onMusicPreferenceChange += OnMusicPreferenceChange;
        string settingJson = PlayerPrefs.GetString(SETTING_KEY, JsonUtility.ToJson(new Setting()));
        Setting settingData = JsonUtility.FromJson<Setting>(settingJson);
        OnMusicPreferenceChange(settingData.Music);
    }

    // Update is called once per frame
    void OnMusicPreferenceChange(bool updatedStatus) {
        if(!updatedStatus)
        {
            musicSource.Pause();
        }
        else
        {
            musicSource.UnPause();
        }

    }

	public void playSFX(int id)
	{
        //play sound effect by id
        if (UIManager.Instance.settingUI.settingData.Sound)
        {
            this.GetComponent<AudioSource>().PlayOneShot(sfx[id]);
        }
	}

	public void stopSFX()
	{
		this.GetComponent<AudioSource>().Stop();
	}
}
