﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{
    public GameObject target;
    public Vector3 offset;
     
    Vector3 velocity = Vector3.zero;
    Vector3 initialPosition = Vector3.zero;
    Player player;

    private void Start()
    {
        player = GetComponent<Player>();
        initialPosition = transform.position;
        GameOverHandler.onSkip -= Reset;
        GameOverHandler.onSkip += Reset;    
    }

    private void Reset()
    {
        transform.position = initialPosition;
    }

    void LateUpdate()
    {
        if (target && GameManager.Instance.CanFollow() )
        {
            Vector3 desiredPosition = target.transform.position + offset;
            desiredPosition.z = -10;
            desiredPosition.x = 0;
            transform.position = desiredPosition;//Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothSpeed);
        }
    }
}