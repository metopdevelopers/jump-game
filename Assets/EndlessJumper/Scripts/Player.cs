﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{

    public bool isMouseControl;
    public bool iseligibleForInteraction ;

    public GameManager Game;
    public GameObject jumpBlue;
    public GameObject jumpOrange;

    public Sprite leftSprite;
    public Sprite rightSprite;

    public SoundManager SFXManager;
    float leftBorder;
    float rightBorder;

    int currentDirection;
    float timeDirection;

    SpriteRenderer thisRenderer;
    // Use this for initialization
    static Controls controls;
    public static int controlID = 0;

    public Animator playerAnimator;
    Rigidbody2D _rigidbody2D;

    Vector2 initialPosition = Vector2.zero;
    Vector2 initialScale = Vector2.zero;
    public Camera gamePlayCamera;

    private void Start()
    {
        initialPosition = transform.position;
        initialScale = transform.localScale;
        iseligibleForInteraction = false;
    }
    public void InitilizePlayer()
    {
        playerAnimator = GetComponent<Animator>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        iseligibleForInteraction = false;
        jumpStarted = false;
        CountDown.readyGoEnd = ReadyGoEnd;
        thisRenderer = this.GetComponent<SpriteRenderer>();

        Controls userSelectedControl = UIManager.Instance.settingUI.settingData.Controls;
        UpdateControl((int)userSelectedControl);
        //UpdateControl((int)1);

#if UNITY_IOS
			Application.targetFrameRate = 60;
#endif
        this.transform.localScale = initialScale * ScaleFactor.GetScaleFactor();
        Vector3 playerSize = GetComponent<Renderer>().bounds.size;
        // Here is the definition of the boundary in world point
        float distance = (transform.position - Camera.main.transform.position).z;
        leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance)).x + (playerSize.x / 2);
        rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance)).x - (playerSize.x / 2);
        timeDirection = Time.time;

        ScreenWidth = Screen.width;

        GameOverHandler.onRestart -= OnRestart;
        GameOverHandler.onRestart += OnRestart;

        ResetPlayer();
    }

    public void OnRestart()
    {
        LeanTween.cancel(lT.id);
        GameManager.Instance.OnRestart();
        gameObject.SetActive(true);
        GetComponent<BoxCollider2D>().enabled = true;
        playerDead = false;
        Reset();
        transform.position = deadPosition;
        //gamePlayCamera.transform.position = cameraPositionWhenDead;
        jump(4);
    }

    void ResetPlayer()
    {
        gameObject.SetActive(true);
        playerMaxPosition = 0;
        transform.position = initialPosition;
        GetComponent<BoxCollider2D>().enabled = true;
        playerDead = false;
        Reset();
    }

    bool isHeadJump = true;

    private void ReadyGoEnd()
    {
        GameManager.Instance.readyForJump = true;
        if (isHeadJump)
        {
            jump(6f);
        }
        else
        {
            jump(4f);
        }
    }

    public static void UpdateControl(int v)
    {
        controls = (Controls)v;
        Debug.Log(controls);
    }

    Vector3 mousePosition;
    private float ScreenWidth;
    float testVar = 0;

    public enum DIRECTION
    {
        NONE, LEFT, RIGHT
    }
    DIRECTION lastDirection = DIRECTION.NONE;
    float timeRemaining = 4f;
    void Update()
    {
        if (!GameManager.Instance.startControl)
        {
            return;
        }
        if(GameManager.Instance.readyForJump && !iseligibleForInteraction)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                iseligibleForInteraction = true;
                //Debug.Log("Now start interaction");

            }
        }
       
        switch (controls)
        {
            case Controls.ONEHANDED:
                HandleOneHandedControl();
                break;
            case Controls.TWOHANDED:
                HandleTwoHandedControl();
                break;
            case Controls.TILT:
                HandleTiltControl();
                break;
            default:
                break;
        }
        if (GameManager.Instance.readyForJump)
        {
            if (Input.GetMouseButtonUp(0))
            {
                playerAnimator.SetBool("exit", true);
                Reset();
            }
        }

        if (playerMaxPosition <= transform.position.y)
        {
            playerMaxPosition = transform.position.y;
        }

        if (GameManager.Instance.readyForJump)
        {
            CheckPlayerDead();
        }

        PlatformGenerator.Instance.UpdateTileCounter();
    }
    private void HandleTiltControl()
    {
        Vector3 acc = Input.acceleration;
        acc.y = 0f;
        acc.z = 0f;
        Debug.Log("Acceleration value" + acc.x);
        Vector3 diff = Vector3.MoveTowards(this.transform.localPosition, this.transform.localPosition + acc, (0.5f * Time.time));

        if (!jumpStarted)
        {
            if (acc.x != 0 && acc.x < 0)
            {
                Debug.Log("Dragging Left");
                PlayLeftAnimation();
            }
            else if (acc.x != 0 && acc.x > 0)
            {
                Debug.Log("Dragging Right");
                PlayRightAnimation();
            }
        }

        diff.y = this.transform.localPosition.y;
        diff.z = 0f;

        if (diff.x < leftBorder - 1f)
        {
            diff.x = rightBorder;
        }
        else if (diff.x > rightBorder + 1f)
        {
            diff.x = leftBorder;
        }

        this.transform.localPosition = diff;
    }

    Vector3 lastTouchedX = Vector3.zero;

    private void HandleOneHandedControl()
    {
        if (Input.GetMouseButtonDown(0))
        {
            lastTouchedX = Input.mousePosition;
            //Debug.LogError("Initial X : " + lastTouchedX);
        }

        if (Input.GetMouseButton(0))
        {
            mousePosition = Input.mousePosition;
            //Debug.Log("Mouse pressed and drag " + mousePosition.x);
            //Debug.LogError("Difference : " + (mousePosition.x - lastTouchedX.x));
            if (!jumpStarted)
            {
                float differene = mousePosition.x - lastTouchedX.x;
                if ((int)differene != 0 && lastTouchedX.x >= mousePosition.x)
                {
                    //Debug.Log("Dragging Left");
                    PlayLeftAnimation();
                }
                else if ((int)differene != 0 && lastTouchedX.x < mousePosition.x)
                {
                    //Debug.Log("Dragging Right");
                    PlayRightAnimation();
                }
            }
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

            mousePosition.y = 0f;
            Vector3 diff2 = Vector3.zero;
            diff2 = Vector3.MoveTowards(this.transform.localPosition, mousePosition, (0.1f * Time.time));
            diff2.y = this.transform.localPosition.y;
            diff2.z = 0f;

            if (diff2.x < leftBorder - 1f)
            {
                diff2.x = rightBorder;
            }
            else if (diff2.x > rightBorder + 1f)
            {
                diff2.x = leftBorder;
            }
            this.transform.localPosition = diff2;
            lastTouchedX = Input.mousePosition;
        }
    }

    private void HandleTwoHandedControl()
    {
        if (Input.GetMouseButton(0))
        {
            if (Input.mousePosition.x > ScreenWidth / 2)
            {
                if (lastDirection == DIRECTION.LEFT || lastDirection == DIRECTION.NONE)
                {
                    lastDirection = DIRECTION.RIGHT;
                }
                testVar += Time.deltaTime * 10;
                PlayRightAnimation();
            }
            else if (Input.mousePosition.x <= ScreenWidth / 2)
            {
                if (lastDirection == DIRECTION.RIGHT || lastDirection == DIRECTION.NONE)
                {
                    lastDirection = DIRECTION.LEFT;
                }
                testVar -= Time.deltaTime * 10;
                PlayLeftAnimation();
            }
            Vector3 diff2 = Vector3.zero;
            diff2 = Vector3.MoveTowards(this.transform.localPosition, new Vector2(testVar, 0), (0.1f * Time.time));

            diff2.y = this.transform.localPosition.y;
            diff2.z = 0f;

            if (diff2.x < leftBorder - 1f)
            {
                diff2.x = rightBorder;
                testVar = rightBorder;
            }
            else if (diff2.x > rightBorder + 1f)
            {
                diff2.x = leftBorder;
                testVar = leftBorder;
            }
            this.transform.localPosition = diff2;
        }

    }

    public void switchOn(string name)
    {
        if (name.Contains("j2")) //Switch on the jump sprite behind the player
            jumpBlue.SetActive(true);
        else
            jumpOrange.SetActive(true);
    }
    public void switchOff(string name)
    {
        if (name.Contains("j2"))//Switch on the jump sprite behind the player
            jumpBlue.SetActive(false);
        else
            jumpOrange.SetActive(false);
    }

    public void jump(float x)
    {
        //Jump (12*x) force - change force for lower jump or change tile jump in the tile.cs script
        _rigidbody2D.velocity = Vector2.zero;
        _rigidbody2D.AddForce(new Vector2(0f, 16.5f * x), ForceMode2D.Impulse);
        PlayJumpAnimation();
        SoundManager.Instance.playSFX(0);//Play Jump Sound
    }

    public void SpeedUp(float x)
    {
        _rigidbody2D.velocity = Vector2.zero;
        _rigidbody2D.AddForce(new Vector2(0f, 16.5f * x), ForceMode2D.Impulse);

        //_rigidbody2D.velocity = _rigidbody2D.velocity * 1.01f;

        PlayJumpAnimation();
        //SoundManager.Instance.playSFX(0);//Play Jump Sound
    }



    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.name.Contains("platform"))
        {
            //Jump if the object is platform
            //Game.player.jump(1);
        }

        else if (_rigidbody2D.velocity.y <= 0)
        {
            ITile tile = col.GetComponent<ITile>();
            if (col.gameObject.name.Contains("Floor"))
            {
                //If the player hits the floor object, then it means he has fallen - end game
                //Game.endGame();
            }
            else if (tile != null)
            {
                tile.PerformAction();
                //PlatformGenerator.Instance.UpdateTileCounter();
            }
        }
    }

    private bool playerDead = false;

    public bool IsPlayerDead()
    {
        return playerDead;
    }
    Vector2 deadPosition = Vector2.zero;
    Vector2 cameraPositionWhenDead = Vector2.zero;

    float playerMaxPosition = 0;
    LTDescr lT;
    public void CheckPlayerDead()
    {
        if (playerDead)
            return;

        if (_rigidbody2D.velocity.y <= 0)
        {
            if (playerMaxPosition - _rigidbody2D.position.y >= 10)
            {
                //Debug.LogError("Player is dead");

                deadPosition = transform.position;
                cameraPositionWhenDead = gamePlayCamera.transform.position;

                GetComponent<BoxCollider2D>().enabled = false;
                lT = LeanTween.delayedCall(2f, () =>
                {
                    gameObject.SetActive(false);
                });
                //If the player hits the floor object, then it means he has fallen - end game
                Game.ShowGameOver();
                playerDead = true;
                return;
            }
        }
        playerDead = false;
    }

    public void PlayRightAnimation()
    {
        if (!jumpStarted)
        {
            Reset();
            playerAnimator.SetBool("right", true);
        }
    }

    public void PlayLeftAnimation()
    {
        if (!jumpStarted)
        {
            Reset();
            playerAnimator.SetBool("left", true);
        }
    }

    private bool jumpStarted = false;
    public void PlayJumpAnimation()
    {
        Reset();
        jumpStarted = true;
        playerAnimator.SetBool("exit", false);
        playerAnimator.SetBool("fly", true);
    }

    public void Reset()
    {
        if (!playerAnimator)
        {
            return;
        }
        playerAnimator.SetBool("left", false);
        playerAnimator.SetBool("right", false);
        playerAnimator.SetBool("fly", false);
    }
}
