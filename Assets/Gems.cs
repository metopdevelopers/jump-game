﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gems : MonoBehaviour
{
    void Start()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DoAction(other);
    }

    private void DoAction(Collider2D other)
    {
        //Debug.LogError(other);
        //if (other.name.Contains("Player"))
        {
            Player player = other.GetComponent<Player>();
            if (player && player.iseligibleForInteraction)
            {
                //Debug.Log("Adding jump");
                player.SpeedUp(2f);
            }
        }
        ScoreManager.Instance.UpdateGems(1);
        gameObject.SetActive(false);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        DoAction(other);
        SoundManager.Instance.playSFX(4);
    }
}
